import React from "react";
import {ItemSprite} from "../item/item-sprite";
import "./player-inventory.css";

export function PlayerInventory(props: PlayerInventoryProps) {
    const {inventory} = props;
    return <React.Fragment>
        <div className={"inventory-title"}>Inventory</div>
        <div className={"inventory"}>
            {
                Object.keys(inventory).map(id => <ItemSprite key={id} id={id} quantity={inventory[id]}/>)
            }
        </div>
    </React.Fragment>;
}

interface PlayerInventoryProps {
    inventory: Record<number, number>
}