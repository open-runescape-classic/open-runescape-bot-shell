import React from "react";
import "./player-message-history.css";

export function PlayerMessageHistory(props: PlayerMessageHistoryProps) {
    const {messageHistory} = props;
    return <React.Fragment>
        <div className={"message-history-title"}>Messages</div>
        <div className={"message-history"}>
            {
                messageHistory.map(
                    (message, index) =>
                        <div
                            className={"message"}
                            key={`${index}-${message}`}>{message}</div>
                )
            }
        </div>
    </React.Fragment>;
}

interface PlayerMessageHistoryProps {
    messageHistory: String[]
}