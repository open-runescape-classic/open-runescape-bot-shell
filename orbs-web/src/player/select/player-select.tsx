import React from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import {Chip} from "@material-ui/core";

export default function PlayerSelect(props: PlayerSelectProps) {
    const {selected, setSelected, usernames} = props;

    return (
        <Autocomplete
            multiple
            id="tags-standard"
            options={usernames}
            defaultValue={selected}
            onChange={(event, newValue) => {
                setSelected(newValue);
            }}
            renderTags={(value: string[], getTagProps) =>
                value.map((option: string, index: number) => (
                    <Chip variant="default" size={"small"} label={option} {...getTagProps({index})} />
                ))
            }
            renderInput={(params) => (
                <TextField
                    {...params}
                    variant="outlined"
                    label="Apply to"
                    placeholder="Usernames/Groups"
                />
            )}
        />
    );
}

interface PlayerSelectProps {
    usernames: string[]
    selected: string[],
    setSelected
        :
        (selected: string[]) => void,
}