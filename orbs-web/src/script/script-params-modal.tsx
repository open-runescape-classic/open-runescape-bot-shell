import React, {useContext, useEffect, useState} from 'react';
import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import classNames from "classnames";
import {getScripts} from "../service/scripts-service";
import {
    Button,
    Checkbox,
    FormControl,
    FormControlLabel,
    InputLabel,
    MenuItem,
    Select,
    TextField,
    Typography
} from "@material-ui/core";
import PlayerSelect from "../player/select/player-select";
import {RegistryContext} from "../context/registry-context";
import {EnumParam, ScriptDefinitionDTO, ScriptParam, ScriptParamType} from "../importedTypes";
import {EnumParamInput} from "./enum-param-input";
import {IRegistry} from "../hooks/useRegistry";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        paper: {
            position: 'absolute',
            width: 400,
            backgroundColor: theme.palette.background.paper,
            color: theme.palette.text.primary,
            boxShadow: theme.shadows[5],
            padding: theme.spacing(2, 4, 3),
        },
        modalCenter: {
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)"
        },
        scriptParams: {
            "& > *": {
                marginBottom: theme.spacing(2)
            }
        },
        modalActions: {
            textAlign: "right"
        }
    }),
);

interface ScriptParamsModalProps {
    isOpen: boolean,
    onCancel: () => void,
    onClose: (scriptName?: string, params?: any, usernames?: string[]) => void,
    username: string,
}

export function ScriptParamsModal(props: ScriptParamsModalProps) {
    const classes = useStyles();
    const {onCancel, onClose, isOpen, username} = props;
    const [scripts, setScripts] = useState<ScriptDefinitionDTO<any>[]>([]);
    const [selectedScript, setSelectedScript] = useState<ScriptDefinitionDTO<any>>();
    const [params, setParams] = useState<any>({});
    const [usernames, setUsernames] = useState<string[]>([username])

    const registry: IRegistry = useContext(RegistryContext);
    const {subjects} = registry;

    useEffect(() => {
        getScripts().then(setScripts);
    }, []);

    const setParamValue = (paramName, value) => {
        setParams({
            ...params,
            [paramName]: value
        })
    }

    const selectScript = (event: React.ChangeEvent<{ value: unknown }>) => {
        const name = event.target.value;
        let script = scripts.find(script => script.name === name);
        setSelectedScript(script);
        setParams(script?.defaultParameters || {});
    };

    const renderParam = (option: ScriptParam, index) => {
        switch (option.type) {
            case ScriptParamType.INSTRUCTIONS:
                return <div key={index}><Typography variant={"caption"}>
                    <Typography variant={"caption"} component={"span"}
                                style={{fontWeight: "bold"}}>Instructions: </Typography>
                    {option.label}
                </Typography></div>
            case ScriptParamType.BOOLEAN:
                return <FormControlLabel
                    control={
                        <Checkbox
                            onChange={evt => setParamValue(option.key, evt.target.checked)}
                            checked={params[option.key]}
                            color={"primary"}
                            key={index}
                        />
                    }
                    label={option.label}
                />
            case ScriptParamType.STRING:
                return <TextField
                    fullWidth={true}
                    variant={"outlined"}
                    value={params[option.key] || ""}
                    onChange={evt => setParamValue(option.key, evt.target.value)}
                    InputLabelProps={{
                        shrink: true,
                    }}
                    label={option.label}
                    key={index}/>;
            case ScriptParamType.NUMBER:
                return <TextField
                    fullWidth={true}
                    variant={"outlined"}
                    type={"number"}
                    value={params[option.key] || ""}
                    onChange={evt =>
                        setParamValue(
                            option.key,
                            evt.target.value && parseInt(evt.target.value)
                        )}
                    InputLabelProps={{
                        shrink: true,
                    }}
                    label={option.label}
                    key={index}
                />
            case ScriptParamType.ENUM:
                return <EnumParamInput
                    key={index}
                    option={option as EnumParam<any>}
                    params={params}
                    setParamValue={setParamValue}
                />
        }
        return <div>Unsupported Parameter: {option.label}</div>
    }

    return (
        <div>
            <Modal
                open={isOpen}
                onClose={onCancel}>
                <div className={classNames(classes.paper, classes.modalCenter)}>
                    <h4>Configure Script</h4>
                    <div className={classes.scriptParams}>
                        <FormControl variant="outlined" fullWidth={true}>
                            <InputLabel id={"script-select-label"} htmlFor={"script-select"}>Script</InputLabel>
                            <Select
                                labelId={"script-select-label"}
                                value={selectedScript?.name || "None"}
                                onChange={selectScript}
                                inputProps={{
                                    id: "script-select"
                                }}
                                label={"Script"}>
                                <MenuItem value={"None"}>None</MenuItem>
                                {scripts?.map((script, index) => <MenuItem
                                    key={index}
                                    value={script?.name}>{script?.name}</MenuItem>)}
                            </Select>
                        </FormControl>
                        <PlayerSelect usernames={subjects} selected={usernames} setSelected={setUsernames}/>
                        {
                            selectedScript &&
                            selectedScript.options.map(renderParam)
                        }
                    </div>
                    <div className={classes.modalActions}>
                        <Button
                            color={"secondary"}
                            onClick={onCancel}>
                            Cancel
                        </Button>
                        <Button
                            color={"primary"}
                            onClick={() => onClose(selectedScript?.name, params, usernames)}>
                            Submit
                        </Button>
                    </div>
                </div>
            </Modal>
        </div>
    );
}