import {Avatar, Chip, FormControl, FormLabel} from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import React from "react";
import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";
import {itemUrlFromId} from "../item/item-sprite";
import {EnumParam, SelectableValue} from "../importedTypes";

interface EnumParamProps {
    option: EnumParam<any>,
    params: any,
    setParamValue: (key: string, value: any) => void
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        enumParam: {
            "& > *": {
                margin: theme.spacing(.25)
            }
        },
        itemAvatar: {
            width: "auto",
            marginLeft: "6px",
            height: "18px",
            backgroundColor: "transparent !important"
        }
    }),
);

export function EnumParamInput(props: EnumParamProps) {
    const {option, params, setParamValue} = props;
    const classes = useStyles();

    const getEnumAvatar = (value: SelectableValue<any>) => {
        if (value.hasOwnProperty("itemId")) {
            return <Avatar
                className={classes.itemAvatar}
                src={itemUrlFromId((value as any).itemId)}/>
        }
        return <></>;
    }

    return <FormControl variant={"filled"} fullWidth={true}>
        <FormLabel className={"MuiInputLabel-shrink"}>{option.label}</FormLabel>
        <div className={classes.enumParam}>
            {
                option.values.map(
                    (item: SelectableValue<any>) => {
                        let selected = (params[option.key] || []).includes(item.value);
                        if (selected) {
                            let removeOption = () => {
                                if (option.multiple) {
                                    setParamValue(
                                        option.key,
                                        params[option.key].filter(val => item.value !== val)
                                    )
                                } else {
                                }
                            };
                            return <Chip
                                key={item.value}
                                avatar={getEnumAvatar(item)}
                                label={item.label}
                                size={"small"}
                                color={"primary"}
                                clickable={true}
                                onClick={removeOption}
                                onDelete={removeOption}
                            />
                        } else {
                            const addOption = () => {
                                if (option.multiple) {
                                    setParamValue(
                                        option.key,
                                        [
                                            ...(params[option.key] || []),
                                            item.value
                                        ]
                                    )
                                } else {
                                    setParamValue(
                                        option.key,
                                        item.value
                                    )
                                }
                            };
                            return <Chip
                                key={item.value}
                                avatar={getEnumAvatar(item)}
                                label={item.label}
                                size={"small"}
                                color={"primary"}
                                variant={"outlined"}
                                clickable={true}
                                deleteIcon={<AddIcon/>}
                                onClick={addOption}
                                onDelete={addOption}
                            />
                        }
                    }
                )
            }
        </div>
    </FormControl>
}