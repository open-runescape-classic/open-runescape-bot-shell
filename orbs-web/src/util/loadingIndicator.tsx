import {makeStyles} from "@material-ui/core/styles";
import Particles from 'react-particles-js';
import {Box} from "@material-ui/core";
import classNames from "classnames";

const useStyles = makeStyles(theme => {
    return {
        hidden: {
            display: "none !important"
        },
        particles: {
            backgroundColor: "#172a18",
            opacity: "60%",
            overflow: "hidden"
        }
    }
})

export function LoadingIndicator({show}) {
    const styles = useStyles();
    return <Box
        className={classNames({[styles.hidden]: !show})}
        top={0}
        left={0}
        right={0}
        bottom={0}
        position={"absolute"}
        overflow={"hidden"}
        zIndex={99999}>
        <Particles
            width={"100%"}
            height={"100%"}
            className={styles.particles}
            params={{
                particles: {
                    number: {
                        value: 60,
                        density: {
                            enable: true,
                            value_area: 1000,
                        }
                    }
                },
                interactivity: {
                    detect_on: "canvas",
                    events: {
                        onhover: {
                            enable: true,
                            mode: "grab"
                        },
                    },
                    modes: {
                        grab: {
                            distance: 191.80819180819182,
                            line_linked: {
                                opacity: 1
                            }
                        }
                    }
                }
            }}/>
    </Box>
}