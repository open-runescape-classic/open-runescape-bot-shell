import React from 'react';
import './App.css';
import {makeStyles} from "@material-ui/core/styles";
import {RegistryContext} from "./context/registry-context";
import {ManagedPlayerContext} from "./context/managed-player-context";
import {OrbsMenu} from "./menu/orbs-menu";
import {LoadingIndicator} from "./util/loadingIndicator";
import {IManagedPlayers, useManagedPlayers} from "./hooks/useManagedPlayers";
import {ORBSDashboard} from "./pages/orbs-dashboard";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import {ORBSRegistry} from "./pages/orbs-registry";
import {Logo} from "./logo";
import {IRegistry, useRegistry} from "./hooks/useRegistry";

const useStyles = makeStyles(() => {
    return {
        root: {
            display: 'flex',
        },
        main: {
            flexGrow: 1,
            justifyContent: "center",
        },
        logo: {
            textAlign: "center",
            padding: "10px",
        },
    }
})

function App() {
    const styles = useStyles();
    const managedPlayers: IManagedPlayers = useManagedPlayers();
    const registry: IRegistry = useRegistry();

    return (
        <ManagedPlayerContext.Provider value={managedPlayers}>
            <RegistryContext.Provider value={registry}>
                <LoadingIndicator show={false}/>
                <div className={styles.root}>
                    <BrowserRouter>
                        <OrbsMenu/>
                        <main className={styles.main}>
                            <div className={styles.logo}>
                                <Logo/>
                            </div>
                            <Switch>
                                <Route exact path={"/"} component={ORBSDashboard}/>
                                <Route path={"/dashboard"} component={ORBSDashboard}/>
                                <Route path={"/registry"} component={ORBSRegistry}/>
                            </Switch>
                        </main>
                    </BrowserRouter>
                </div>
            </RegistryContext.Provider>
        </ManagedPlayerContext.Provider>
    );
}

export default App;
