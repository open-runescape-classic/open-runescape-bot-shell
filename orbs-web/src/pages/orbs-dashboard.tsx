import {makeStyles} from "@material-ui/core/styles";
import {TextField} from "@material-ui/core";
import {Keyframes} from "../keyframes";
import {Player} from "../player/player";
import React, {useContext, useEffect, useState} from "react";
import {ManagedPlayerContext} from "../context/managed-player-context";

const useStyles = makeStyles(theme => ({
    actions: {
        margin: theme.spacing(1),
        padding: theme.spacing(1),
        textAlign: "center"
    },
    visible: {
        animation: "dropAndFadeIn 1s ease-out",
        opacity: "100% !important"
    },
    options: {
        margin: "10px",
        padding: "10px",
        marginLeft: "auto",
        marginRight: "auto",
        maxWidth: 600,
        "& > *": {
            marginLeft: "auto",
            marginRight: "auto"
        }
    }
}));

const animateFrom = {
    top: "-15px",
    opacity: "0%",
}

const animateTo = {
    top: "0",
    opacity: "100%",
}

export function ORBSDashboard() {
    const [filter, setFilter] = useState<string>();
    const {players} = useContext(ManagedPlayerContext);
    useEffect(() => {
        let i = 0;
        players.forEach(player => {
            let playerEl = document.getElementById(player.username);
            if (!playerEl?.className?.includes(styles.visible)) {
                setTimeout(() => {
                    if (Boolean(playerEl)) {
                        playerEl.className += " " + styles.visible;
                    }
                }, i++ * 170);
            }
        })
    }, [players])
    const styles = useStyles();
    return <>
        <div className={styles.actions}>
        </div>
        <div className={styles.options}>
            <TextField
                variant={"outlined"}
                label={"Query"}
                placeholder={"Query Players: ex. bank(155) > 20 and level(\"mining\") > 60"}
                onChange={evt => setFilter(evt.target.value)}
                value={filter}
                size={"medium"}
                fullWidth={true}
            />
        </div>
        <div className="players">
            <Keyframes name={"dropAndFadeIn"}
                       from={animateFrom}
                       to={animateTo}/>
            {players.map(player => <Player id={player.username} key={player.username} player={player}/>)}
        </div>
    </>;
}