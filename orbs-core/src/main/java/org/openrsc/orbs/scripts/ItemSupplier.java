package org.openrsc.orbs.scripts;

import com.google.common.eventbus.Subscribe;
import lombok.Builder;
import lombok.Getter;
import lombok.experimental.FieldNameConstants;
import org.openrsc.orbs.model.Point;
import org.openrsc.orbs.net.connection.player.script.ItemIds;
import org.openrsc.orbs.net.connection.player.script.Script;
import org.openrsc.orbs.net.connection.player.script.ScriptAPI;
import org.openrsc.orbs.net.connection.player.script.event.ScriptEvent;
import org.openrsc.orbs.net.connection.player.script.model.Item;
import org.openrsc.orbs.net.connection.player.script.model.ItemSet;
import org.openrsc.orbs.net.connection.player.script.params.ScriptParam;
import org.openrsc.orbs.net.connection.player.script.params.StringParam;

import java.util.List;

public class ItemSupplier extends Script<ItemSupplier.Params> {
    private ItemSet itemSet = ItemSet.builder().build();
    private Point location;
    private int freeSpaces = 30;

    public ItemSupplier(ScriptAPI api) {
        super(api);
    }

    @Override
    public void init(Params params) {
        super.init(params);
        itemSet = getItemSetFromString(params.getItemSetAsString());
    }

    private ItemSet getItemSetFromString(String itemSetAsString) {
        final ItemSet.ItemSetBuilder builder = ItemSet.builder();
        String[] items = itemSetAsString.split(",");
        for (String item : items) {
            String[] split = item.split(":");
            int itemId = Integer.parseInt(split[0]);
            int itemCount = Integer.parseInt(split[1]);
            builder.item(Item.builder().id(itemId).amount(itemCount).build());
        }
        return builder.build();
    }

    @Override
    public void run() {
        if (location == null) {
            api.broadcast(
                    RequestLocationEvent.builder()
                            .targetPlayer(getParams().getTargetPlayer())
                            .build()
            );
            api.pause(1000);
        } else if (location.distance(api.getLocation()) > 10) {
            api.walkTo(location);
        } else if (api.itemSetInInventory(itemSet) > 0) {
            api.getPlayerByUsername(getParams().getTargetPlayer()).ifPresent(player -> {
                api.walkTo(player);
                api.sendTradeRequest(player.getPlayerId());
                api.pauseUntil(api::isTrading, 3000);
                if (api.isTrading()) {
                    int opponentFreeSpaceMultiplier = Math.min(freeSpaces, 12) / itemSet.getTotalCost();
                    int inventoryMultipler = api.itemSetInInventory(itemSet);
                    int multiplier = Math.min(opponentFreeSpaceMultiplier, inventoryMultipler);
                    api.setTradeOffer(itemSet.multiply(multiplier));
                    while (api.isTrading()) {
                        api.acceptTrade();
                    }
                }
            });
        }
    }

    @Override
    public void shutdown() {
        api.walkToNearestBank();
        api.openBank();
        api.depositAllExcept(ItemIds.SLEEPING_BAG.id());
        api.closeBank();
        done();
    }

    @Override
    public Params getDefaultParameters() {
        return Params.builder().build();
    }

    @Override
    public List<ScriptParam> getOptions() {
        return List.of(
                StringParam.builder()
                        .key(Params.Fields.targetPlayer)
                        .label("Receiving Player's Username")
                        .build(),
                StringParam.builder()
                        .key(Params.Fields.itemSetAsString)
                        .label("Item Set in itemId:count (i.e. 151:1,155:2)")
                        .build()
        );
    }

    @Subscribe
    public void onLocationReceived(ItemAcceptor.MyLocationEvent event) {
        if (getParams().getTargetPlayer().equalsIgnoreCase(event.getSender())) {
            location = event.getLocation();
        }
    }

    @Subscribe
    public void onFreeSpacesReceived(ItemAcceptor.FreeSpacesEvent event) {
        if (event.getTargetPlayer().equalsIgnoreCase(api.getUsername())) {
            freeSpaces = event.getFreeSpaces();
        }
    }

    @Builder
    @Getter
    @FieldNameConstants
    public static class Params {
        private final String targetPlayer;
        private final String itemSetAsString;
    }

    @Builder
    @Getter
    public static class RequestLocationEvent extends ScriptEvent {
        private final String targetPlayer;
    }
}
