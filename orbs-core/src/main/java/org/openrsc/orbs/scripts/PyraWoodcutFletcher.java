package org.openrsc.orbs.scripts;

import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.experimental.FieldNameConstants;
import org.openrsc.orbs.net.connection.player.script.CommonIds;
import org.openrsc.orbs.net.connection.player.script.ItemIds;
import org.openrsc.orbs.net.connection.player.script.Script;
import org.openrsc.orbs.net.connection.player.script.ScriptAPI;
import org.openrsc.orbs.net.connection.player.script.params.EnumParam;
import org.openrsc.orbs.net.connection.player.script.params.InstructionsParam;
import org.openrsc.orbs.net.connection.player.script.params.Itemable;
import org.openrsc.orbs.net.connection.player.script.params.NumberParam;
import org.openrsc.orbs.net.connection.player.script.params.ScriptParam;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class PyraWoodcutFletcher extends Script<PyraWoodcutFletcher.Params> {
    private final Map<String, String> metrics = new ConcurrentHashMap<>();

    public PyraWoodcutFletcher(ScriptAPI api) {
        super(api);
    }

    @Override
    public void init(Params params) {
        super.init(params);
        int axeCount = api.getInventoryCount(
                ItemIds.BRONZE_AXE.id(),
                ItemIds.IRON_AXE.id(),
                ItemIds.STEEL_AXE.id(),
                ItemIds.BLACK_AXE.id(),
                ItemIds.MITHRIL_AXE.id(),
                ItemIds.ADAMANTITE_AXE.id(),
                ItemIds.RUNE_AXE.id()
        );
        int sleepingBagCount = api.getInventoryCount(ItemIds.SLEEPING_BAG.id());
        if (axeCount == 0 || sleepingBagCount == 0) {
            api.displayMessage("Warning: This script requires an axe and sleeping bag.");
        }
    }

    @Override
    public void run() {
        api.sleepIfFatigueAbove(getParams().getFatigueLimit());
        api.findItemInInventory(ItemIds.LOGS.id()).ifPresentOrElse(
                logSlot -> api.findItemInInventory(ItemIds.KNIFE.id()).ifPresent(knifeSlot -> {
                    api.useItemOnItem(knifeSlot, logSlot);
                    api.pauseUntil(api::inMenu, 3000);
                    api.chooseOption(getParams().getMode().getOptionId());
                    api.pause(300);
                }),
                () -> api.findNearestObject(CommonIds.OBJ_TREES).ifPresent(api::useObject)
        );
    }

    @Override
    public void shutdown() {
        api.walkToNearestBank();
        api.openBank();
        api.depositAllExcept(ItemIds.SLEEPING_BAG.id());
        api.closeBank();
        done();
    }

    @Override
    public Map<String, String> getMetrics() {
        return metrics;
    }

    @Override
    public Params getDefaultParameters() {
        return Params.builder().build();
    }

    @Override
    public List<ScriptParam> getOptions() {
        return Lists.newArrayList(
                InstructionsParam.builder()
                        .key("instructions")
                        .label("Start near lots of trees preferably")
                        .build(),
                NumberParam.builder()
                        .key(Params.Fields.fatigueLimit)
                        .label("Fatigue Limit")
                        .build(),
                EnumParam.<FletchMode>builder()
                        .values(FletchMode.values())
                        .key(Params.Fields.mode)
                        .label("Mode")
                        .build()
        );
    }

    @AllArgsConstructor
    @Getter
    enum FletchMode implements Itemable {
        SHAFTS(ItemIds.ARROW_SHAFTS.id(), "Arrow Shafts", 0),
        SHORT_BOW(ItemIds.SHORTBOW.id(), "Short Bow", 1),
        LONG_BOW(ItemIds.LONGBOW.id(), "Long Bow", 2);

        private final Integer itemId;
        private final String label;
        private final Integer optionId;

        @Override
        public String toString() {
            return getLabel();
        }
    }

    @Builder
    @Getter
    @FieldNameConstants
    public static class Params {
        @Builder.Default
        final FletchMode mode = FletchMode.SHAFTS;
        @Builder.Default
        final int fatigueLimit = 95;
    }
}
