package org.openrsc.orbs.stateful.trade;

public enum TradeStatus {
    NOT_TRADING,
    OFFER_WINDOW,
    CONFIRM_WINDOW
}
