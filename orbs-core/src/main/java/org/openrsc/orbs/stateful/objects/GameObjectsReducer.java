package org.openrsc.orbs.stateful.objects;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import lombok.extern.log4j.Log4j2;
import org.openrsc.orbs.model.Direction;
import org.openrsc.orbs.model.Point;
import org.openrsc.orbs.model.TileTraversal;
import org.openrsc.orbs.model.data.DoorDef;
import org.openrsc.orbs.model.data.GameObjectDef;
import org.openrsc.orbs.net.dto.server.objects.BoundaryUpdateEvent;
import org.openrsc.orbs.net.dto.server.objects.SceneryUpdateEvent;
import org.openrsc.orbs.net.util.DataConversions;
import org.openrsc.orbs.stateful.AbstractReducer;
import org.openrsc.orbs.stateful.player.PlayerState;

import java.util.Map;
import java.util.Optional;

import static org.openrsc.orbs.model.Direction.*;

@Log4j2
public class GameObjectsReducer extends AbstractReducer {
    private final GameObjectsState gameObjectsState;
    private final PlayerState playerState;
    private final Map<Point, TileTraversal> objectTraversalMap;

    public GameObjectsReducer(PlayerState playerState, GameObjectsState gameObjectsState, EventBus eventBus) {
        super(eventBus);
        this.gameObjectsState = gameObjectsState;
        this.playerState = playerState;
        objectTraversalMap = playerState.getObjectTraversalMap();
    }

    @Subscribe
    public void onBoundariesUpdates(BoundaryUpdateEvent event) {
        event.getObjects().forEach(object -> {
            Point actualLocation = DataConversions.reverseCoordOffsets(
                    object.getOffsetX(),
                    object.getOffsetY(),
                    playerState.getLocation()
            );
            GameObject gameObj = GameObject.fromDTO(object, actualLocation);
            Map<Point, GameObject> boundaries = gameObjectsState.getBoundaries();

            GameObject existingBoundary = boundaries.get(actualLocation);

            boundaries.put(
                    actualLocation,
                    gameObj
            );

            if (existingBoundary != null) {
                if (!existingBoundary.equals(gameObj)) {
                    removeBoundaryObject(existingBoundary);
                } else {
                    return;
                }
            }
            registerBoundaryObject(gameObj);
        });
    }

    @Subscribe
    public void onSceneriesUpdates(SceneryUpdateEvent event) {
        event.getObjects().forEach(object -> {
            Point actualLocation = DataConversions.reverseCoordOffsets(
                    object.getOffsetX(),
                    object.getOffsetY(),
                    playerState.getLocation()
            );
            GameObject gameObj = GameObject.fromDTO(object, actualLocation);
            Map<Point, GameObject> sceneries = gameObjectsState.getSceneries();

            GameObject existingScenery = sceneries.get(actualLocation);
            if (existingScenery != null && !existingScenery.equals(gameObj)) {
                removeSceneryObject(existingScenery);
            }

            sceneries.put(
                    actualLocation,
                    gameObj
            );
            registerSceneryObject(gameObj);
        });
    }

    public TileTraversal getTileTraversal(int x, int y) {
        return getTileTraversal(new Point(x, y));
    }

    private TileTraversal getTileTraversal(Point point) {
        return objectTraversalMap.computeIfAbsent(
                point,
                TileTraversal::new
        );
    }

    private void setDirectionBlocked(Point point, Direction direction, boolean blocked) {
        getTileTraversal(point).setIsBlocked(direction, blocked);
        getTileTraversal(point.move(direction))
                .setIsBlocked(direction.inverse(), blocked);
    }

    public void registerSceneryObject(GameObject obj) {
        GameObjectDef def = GameObjectDef.getObjectDef(obj.getId());
        Point location = obj.getLocation();
        int direction = obj.getDirection();

        if (
                def == null
                        || def.getModelID() == 1147
                        || (def.getType() != 1 && def.getType() != 2)
        ) {
            return;
        }

        int width, height;
        if (direction == 0 || direction == 4) {
            width = def.getWidth();
            height = def.getHeight();
        } else {
            height = def.getWidth();
            width = def.getHeight();
        }
        for (int x = location.getX(); x < location.getX() + width; ++x) {
            for (int y = location.getY(); y < location.getY() + height; ++y) {
                Point point = new Point(x, y);
                TileTraversal traversal = getTileTraversal(x, y);
//                if (isProjectileClipAllowed(o)) {
//                    handleProjectileClipAllowance(x, y, dir, o.getType(), def.getType(), -1);
//                }
                if (def.getType() == 1) {
                    traversal.setTraversable(false);
                } else if (direction == 0) {
                    setDirectionBlocked(point, EAST, true);
                } else if (direction == 2) {
                    setDirectionBlocked(point, SOUTH, true);
                } else if (direction == 4) {
                    setDirectionBlocked(point, WEST, true);
                } else if (direction == 6) {
                    setDirectionBlocked(point, NORTH, true);
                }
            }
        }
    }

    private void removeSceneryObject(GameObject obj) {
        GameObjectDef def = GameObjectDef.getObjectDef(obj.getId());
        Point location = obj.getLocation();
        int direction = obj.getDirection();

        if (def == null || (def.getType() != 1 && def.getType() != 2)) {
            return;
        }
        int width, height;
        if (direction == 0 || direction == 4) {
            width = def.getWidth();
            height = def.getHeight();
        } else {
            height = def.getWidth();
            width = def.getHeight();
        }
        for (int x = location.getX(); x < location.getX() + width; ++x) {
            for (int y = location.getY(); y < location.getY() + height; ++y) {
                Point point = new Point(x, y);
                if (def.getType() == 1) {
                    getTileTraversal(location).setTraversable(true);
                } else if (direction == 0) {
                    setDirectionBlocked(point, EAST, false);
                } else if (direction == 2) {
                    setDirectionBlocked(point, SOUTH, false);
                } else if (direction == 4) {
                    setDirectionBlocked(point, WEST, false);
                } else if (direction == 6) {
                    setDirectionBlocked(point, NORTH, false);
                }
            }
        }
    }

    public void registerBoundaryObject(GameObject obj) {
        GameObjectDef def = GameObjectDef.getObjectDef(obj.getId());
        Point location = obj.getLocation();
        int direction = obj.getDirection();

        if (def == null || def.getModelID() == 1147) {
            return;
        }

        DoorDef doorDef;
        try {
            doorDef = DoorDef.getDoorDef(obj.getId());
            if (doorDef != null && doorDef.getDoorType() != 1) {
                return;
            }
        } catch (Exception ignored) {
        }

        TileTraversal traversal = getTileTraversal(location);
//        if (isProjectileClipAllowed(o)) {
//            handleProjectileClipAllowance(x, y, dir, o.getType(), -1, doorDef.getDoorType());
//        }
        if (direction == 0) {
            setDirectionBlocked(location, NORTH, true);
        } else if (direction == 1) {
            setDirectionBlocked(location, EAST, true);
        } else if (direction == 2) {
            traversal.setTraversable(false);
        } else if (direction == 3) {
            traversal.setTraversable(false);
        }
    }

    private void removeBoundaryObject(GameObject obj) {
        Optional.ofNullable(GameObjectDef.getObjectDef(obj.getId())).ifPresent(def -> {
            DoorDef doorDef;
            try {
                doorDef = DoorDef.getDoorDef(obj.getId());
                if (doorDef != null && doorDef.getDoorType() != 1) {
                    return;
                }
            } catch (Exception ignored) {
            }
            Point location = obj.getLocation();
            int direction = obj.getDirection();
            TileTraversal traversal = getTileTraversal(location);
            if (direction == 0) {
                setDirectionBlocked(location, NORTH, false);
            } else if (direction == 1) {
                setDirectionBlocked(location, EAST, false);
            } else if (direction == 2) {
                traversal.setTraversable(true);
            } else if (direction == 3) {
                traversal.setTraversable(true);
            }
        });
    }
}
