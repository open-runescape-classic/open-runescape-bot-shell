package org.openrsc.orbs.stateful.config;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import org.openrsc.orbs.Main;
import org.openrsc.orbs.net.dto.server.config.ServerConfigurationEvent;
import org.openrsc.orbs.stateful.AbstractReducer;
import org.openrsc.orbs.stateful.player.PlayerState;

public class ServerConfigurationReducer extends AbstractReducer {
    private final PlayerState playerState;

    public ServerConfigurationReducer(PlayerState playerState, EventBus eventBus) {
        super(eventBus);
        this.playerState = playerState;
    }

    @Subscribe
    public void onServerConfiguration(ServerConfigurationEvent event) {
        playerState.setServerConfiguration(event);
        /*
         * TODO: This is really bad, but I don't have a good way to share this with the protocol layer which has conditional
         * formatting based on configuration
         */
        Main.SERVER_CONFIGURATION = event;
    }
}
