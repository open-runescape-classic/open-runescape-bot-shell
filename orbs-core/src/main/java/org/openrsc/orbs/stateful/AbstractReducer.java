package org.openrsc.orbs.stateful;

import com.google.common.eventbus.EventBus;
import lombok.Getter;

public class AbstractReducer {
    @Getter
    private final EventBus eventBus;

    public AbstractReducer(EventBus eventBus) {
        eventBus.register(this);
        this.eventBus = eventBus;
    }

    public void dispose() {
        eventBus.unregister(this);
    }
}
