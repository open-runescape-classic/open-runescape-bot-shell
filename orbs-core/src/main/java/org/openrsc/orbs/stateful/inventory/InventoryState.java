package org.openrsc.orbs.stateful.inventory;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;
import org.openrsc.orbs.model.InventoryItem;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

@Getter
public class InventoryState {
    @JsonValue
    private final List<InventoryItem> bySlot = new CopyOnWriteArrayList<>();
    @JsonIgnore
    private final Map<Integer, InventoryItem> byId = new ConcurrentHashMap<>();
}
