package org.openrsc.orbs.stateful.skills;

import lombok.Getter;
import lombok.Setter;
import org.openrsc.orbs.model.Skill;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Getter
public class SkillState {
    private final Map<Skill, Integer> currentLevels = new ConcurrentHashMap<>();
    private final Map<Skill, Integer> maxLevels = new ConcurrentHashMap<>();
    private final Map<Skill, Integer> currentExperience = new ConcurrentHashMap<>();
    @Setter
    private volatile Integer questPoints = 0;
}
