package org.openrsc.orbs.controller.dto;

import lombok.Builder;
import lombok.Getter;

@Builder(toBuilder = true)
@Getter
public class StatDTO {
    private final String name;
    private final Integer currentLvl;
    private final Integer maxLvl;
}
