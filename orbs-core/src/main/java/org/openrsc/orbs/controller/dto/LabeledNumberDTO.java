package org.openrsc.orbs.controller.dto;

import lombok.Builder;
import lombok.Getter;

@Builder(toBuilder = true)
@Getter
public class LabeledNumberDTO {
    private final String name;
    private final int level;
}
