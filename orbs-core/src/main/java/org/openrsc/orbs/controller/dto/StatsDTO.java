package org.openrsc.orbs.controller.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Singular;

import java.util.List;

@Builder(toBuilder = true)
@Getter
public class StatsDTO {
    @Singular
    private final List<StatDTO> stats;
}
