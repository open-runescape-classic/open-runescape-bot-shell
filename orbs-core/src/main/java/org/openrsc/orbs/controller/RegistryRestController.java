package org.openrsc.orbs.controller;

import org.openrsc.orbs.database.PersistenceManager;
import org.openrsc.orbs.model.persisted.GroupDefinition;
import org.openrsc.orbs.model.persisted.PlayerDefinition;
import org.openrsc.orbs.service.PlayerService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
@RequestMapping("/registry")
public class RegistryRestController {
    private final PersistenceManager persistenceManager;
    private final PlayerService playerService;

    public RegistryRestController(PersistenceManager persistenceManager, PlayerService playerService) {
        this.persistenceManager = persistenceManager;
        this.playerService = playerService;
    }

    @GetMapping("/player")
    public Set<PlayerDefinition> getAllPlayers() {
        return persistenceManager.getAllPlayers();
    }

    @PostMapping("/player")
    public PlayerDefinition postPlayer(@RequestBody PlayerDefinition playerDefinition) {
        return persistenceManager.savePlayer(playerDefinition);
    }

    @GetMapping("/player/{name}/delete")
    public void deletePlayer(@PathVariable("name") String username) {
        playerService.logout(username);
        persistenceManager.unregisterPlayer(username);
    }

    @GetMapping("/group")
    public Set<GroupDefinition> getAllGroups() {
        return persistenceManager.getAllGroups();
    }

    @GetMapping("/group/{groupName}/{username}")
    public void addPlayerToGroup(@PathVariable String groupName, @PathVariable String username) {
        persistenceManager.addPlayerToGroup(groupName, username);
    }

    @GetMapping("/group/{name}")
    public GroupDefinition getGroup(@PathVariable String name) {
        return persistenceManager.getGroupById(name);
    }
}
