package org.openrsc.orbs.controller.dto;

import lombok.Builder;
import lombok.Getter;
import org.openrsc.orbs.net.connection.player.script.ScriptState;

import java.util.Collection;
import java.util.Map;

@Builder(toBuilder = true)
@Getter
public class ManagedPlayerDTO {
    private final String username;
    private final String status;
    private final String location;
    private final String area;
    private final AppearanceDTO appearance;
    private final Collection<String> messageHistory;
    private final Map<Integer, Integer> inventory;
    private final ConnectionStateDTO connectionState;
    private final long reconnectTimeTotal;
    private final long reconnectTimeRemaining;
    private final long scriptElapsedTime;
    private final ScriptState scriptState;
    private final String errorMessage;
}
