package org.openrsc.orbs.controller;

import lombok.extern.log4j.Log4j2;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.openrsc.orbs.model.Direction;
import org.openrsc.orbs.model.Point;
import org.openrsc.orbs.model.TileTraversal;
import org.openrsc.orbs.model.TileTraversalProvider;
import org.openrsc.orbs.service.PlayerService;
import org.openrsc.orbs.stateful.player.PlayerState;
import org.openrsc.orbs.util.World;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Log4j2
@RestController
@RequestMapping("/world")
public class WorldRestController {
    private static final int ARR_SIZE = 7;
    private final World world;
    private final PlayerService playerService;

    public WorldRestController(World world, PlayerService playerService) {
        this.world = world;
        this.playerService = playerService;
    }

    @GetMapping(value = "/{player}/map")
    public void getMapImage(
            HttpServletResponse response,
            @PathVariable String player,
            @RequestParam("dist") Integer distance
    ) throws IOException {
        PlayerState playerState = playerService.getPlayerState(player);
        TileTraversalProvider tileProvider = playerService.getTileProvider(player);
        Point location = playerState.getLocation();
        int x = location.getX();
        int y = location.getY();

        final int tileSize = 80;
        if (distance == null) {
            distance = 5;
        }

        int size = tileSize * (distance * 2 + 1) + 1;
        BufferedImage image = new BufferedImage(size, size, BufferedImage.TYPE_INT_RGB);
        Graphics g = image.createGraphics();
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, size, size);

        Set<Point> points = new HashSet<>();
        int minX = x - distance;
        int minY = y - distance;
        for (int i = minX; i <= x + distance; i++) {
            for (int j = minY; j <= y + distance; j++) {
                points.add(new Point(i, j));
            }
        }
        Map<Point, TileTraversal> tiles = points.stream().collect(Collectors.toMap(
                point -> point,
                tileProvider::apply
        ));

        Integer finalDistance = distance;
        tiles.forEach((point, tile) -> {
            Point fromOrigin = new Point((point.getX() - minX), point.getY() - minY);
            // Flip x axis (x axis is inverted)
            Point mapped = new Point(finalDistance * 2 - fromOrigin.getX(), fromOrigin.getY());

            g.setColor(Color.LIGHT_GRAY);
            g.drawRect(mapped.getX() * tileSize, mapped.getY() * tileSize, tileSize, tileSize);
            if (point.equals(new Point(x, y))) {
                g.setColor(Color.BLUE);
                g.fillOval(
                        (int) ((mapped.getX() + 0.25) * tileSize),
                        (int) ((mapped.getY() + 0.25) * tileSize),
                        tileSize / 2,
                        tileSize / 2
                );
            }

            if (tile.getMovableDirections(tileProvider).isEmpty()) {
                g.setColor(Color.RED);
                g.fillRect(mapped.getX() * tileSize, mapped.getY() * tileSize, tileSize, tileSize);
            }

            g.setColor(Color.GREEN);
            for (Direction direction : tile.getMovableDirections(tileProvider)) {
                // Invert x
                Direction mappedDirection = Direction.byOffset(new Point(
                        direction.getOffset().inverse().getX(),
                        direction.getOffset().getY()
                ));
                drawArrow(
                        g,
                        (int) ((0.5 + mapped.getX() + (mappedDirection.getOffset().getX() * .35)) * tileSize),
                        (int) ((0.5 + mapped.getY() + (mappedDirection.getOffset().getY() * .35)) * tileSize),
                        (int) ((0.5 + mapped.move(mappedDirection).getX() + mappedDirection.getOffset().inverse().getX() * .35) * tileSize),
                        (int) ((0.5 + mapped.move(mappedDirection).getY() + mappedDirection.getOffset().inverse().getY() * .35) * tileSize)
                );
            }

            Point topLeft = new Point(mapped.getX() * tileSize, mapped.getY() * tileSize);
            Point topRight = new Point(topLeft.getX() + tileSize, topLeft.getY());
            Point bottomLeft = new Point(topLeft.getX(), topLeft.getY() + tileSize);
            Point bottomRight = new Point(topLeft.getX() + tileSize, topLeft.getY() + tileSize);
            if (!tile.canMove(tileProvider, Direction.EAST)) {
                g.setColor(Color.RED);
                g.drawLine(
                        topRight.getX(),
                        topRight.getY(),
                        bottomRight.getX(),
                        bottomRight.getY()
                );
            }
            if (!tile.canMove(tileProvider, Direction.WEST)) {
                g.setColor(Color.RED);
                g.drawLine(
                        topLeft.getX(),
                        topLeft.getY(),
                        bottomLeft.getX(),
                        bottomLeft.getY()
                );
            }
            if (!tile.canMove(tileProvider, Direction.NORTH)) {
                g.setColor(Color.RED);
                g.drawLine(
                        topLeft.getX(),
                        topLeft.getY(),
                        topRight.getX(),
                        topRight.getY()
                );
            }
            if (!tile.canMove(tileProvider, Direction.SOUTH)) {
                g.setColor(Color.RED);
                g.drawLine(
                        bottomLeft.getX(),
                        bottomLeft.getY(),
                        bottomRight.getX(),
                        bottomRight.getY()
                );
            }

            g.setColor(Color.WHITE);
            g.setFont(new Font("Arial", Font.PLAIN, 10));
            g.drawString(
                    "(" + point.getX() + "," + point.getY() + ")",
                    mapped.getX() * tileSize + 20,
                    mapped.getY() * tileSize + 40
            );
        });

        response.setContentType(MediaType.IMAGE_JPEG_VALUE);
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        ImageIO.write(image, "jpeg", os);
        IOUtils.copy(new ByteArrayInputStream(os.toByteArray()), response.getOutputStream());
    }

    void drawArrow(Graphics g1, int x1, int y1, int x2, int y2) {
        Graphics2D g = (Graphics2D) g1.create();

        double dx = x2 - x1, dy = y2 - y1;
        double angle = Math.atan2(dy, dx);
        int len = (int) Math.sqrt(dx * dx + dy * dy);
        AffineTransform at = AffineTransform.getTranslateInstance(x1, y1);
        at.concatenate(AffineTransform.getRotateInstance(angle));
        g.transform(at);

        // Draw horizontal arrow starting in (0, 0)
        g.drawLine(0, 0, len, 0);
        g.fillPolygon(new int[]{len, len - ARR_SIZE, len - ARR_SIZE, len},
                new int[]{0, -ARR_SIZE, ARR_SIZE, 0}, 4);
    }
}
