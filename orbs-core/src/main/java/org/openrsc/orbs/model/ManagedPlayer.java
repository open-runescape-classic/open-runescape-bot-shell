package org.openrsc.orbs.model;

import lombok.Builder;
import lombok.Getter;
import org.openrsc.orbs.controller.dto.AppearanceDTO;

@Builder(toBuilder = true)
@Getter
public class ManagedPlayer {
    private final String username;
    private final boolean enabled;
    private final String status;
    private final AppearanceDTO appearance;
}
