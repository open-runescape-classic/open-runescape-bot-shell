package org.openrsc.orbs.model.persisted;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class PlayerDefinition {
    private final String username;
    private final String password;
    private final boolean active;
}
