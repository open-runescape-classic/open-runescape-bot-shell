package org.openrsc.orbs.model;

public interface Locatable {
    Point getLocation();
}
