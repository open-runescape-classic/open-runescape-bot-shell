package org.openrsc.orbs.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.tuple.Pair;
import org.openrsc.orbs.util.data.BitBoolean;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class TileTraversal {
    private final Point point;
    private BitBoolean<Direction> directionBlocked = new BitBoolean<>();

    @Getter
    @Setter
    private boolean traversable = true;

    public TileTraversal(Point point) {
        this.point = point;
    }

    public static TileTraversal combine(
            Point point,
            TileTraversal traversal,
            TileTraversal traversal2
    ) {
        TileTraversal result = new TileTraversal(point);
        result.traversable = traversal.isTraversable() && traversal2.isTraversable();
        result.directionBlocked = traversal.directionBlocked.or(traversal2.directionBlocked);
        return result;
    }

    public boolean canMove(TileTraversalProvider tileProvider, Direction direction) {
        return canMove(tileProvider, direction, true);
    }

    private boolean canMove(
            TileTraversalProvider tileProvider,
            Direction direction,
            boolean checkDestination) {
        if (!traversable) {
            return false;
        }
        if (!direction.isCardinal()) {
            Pair<Direction, Direction> borderingDirections = direction.getBorderingOrdinals();
            Direction left = borderingDirections.getLeft();
            boolean canMoveLeftSide = !directionBlocked.get(left)
                    && tileProvider.apply(point.move(left)).isTraversable();
            Direction right = borderingDirections.getRight();
            boolean canMoveRightSide = !directionBlocked.get(right)
                    && tileProvider.apply(point.move(right)).isTraversable();
            boolean canMoveImmediate = canMoveLeftSide && canMoveRightSide;

            if (checkDestination) {
                return canMoveImmediate &&
                        tileProvider.apply(point.move(direction))
                                .canMove(tileProvider, direction.inverse(), false);
            }
            return canMoveImmediate;
        }

        TileTraversal destinationTile = tileProvider.apply(point.move(direction));
        if (checkDestination) {
            return !directionBlocked.get(direction)
                    && destinationTile.isTraversable()
                    && destinationTile.canMove(tileProvider, direction.inverse(), false);
        }
        return !directionBlocked.get(direction)
                && destinationTile.isTraversable();

    }

    public void setIsBlocked(Direction direction, boolean isBlocked) {
        directionBlocked.set(direction, isBlocked);
    }

    @JsonInclude
    public List<Direction> getMovableDirections(TileTraversalProvider tileProvider) {
        return Arrays.stream(Direction.values())
                .filter(direction -> canMove(tileProvider, direction))
                .collect(Collectors.toList());
    }

    /**
     * assumes this tile is not traversable, can only be accessed by adjacent points not blocked by wall
     */
    public List<Direction> getAccessibleDirections(TileTraversalProvider tileProvider) {
        return Arrays.stream(Direction.values())
                .filter(direction -> {
                    TileTraversal from = tileProvider.apply(point.move(direction));

                    boolean canMoveImmediate = true;
                    if (!direction.isCardinal()) {
                        Pair<Direction, Direction> borderingDirections = direction.getBorderingOrdinals();
                        Direction left = borderingDirections.getLeft();
                        boolean canMoveLeftSide = !directionBlocked.get(left);
                        Direction right = borderingDirections.getRight();
                        boolean canMoveRightSide = !directionBlocked.get(right);

                        canMoveImmediate = canMoveLeftSide && canMoveRightSide;
                    }

                    return canMoveImmediate
                            && from.isTraversable()
                            && !directionBlocked.get(direction);
                })
                .collect(Collectors.toList());
    }
}
