package org.openrsc.orbs.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.openrsc.orbs.util.World;

@AllArgsConstructor
@Getter
public class Point implements Comparable<Point> {
    private final int x;
    private final int y;

    public static Point fromDTO(org.openrsc.orbs.net.dto.Point point) {
        return new Point(point.getX(), point.getY());
    }

    public org.openrsc.orbs.net.dto.Point toDTO() {
        return new org.openrsc.orbs.net.dto.Point(x, y);
    }

    @Override
    public String toString() {
        return "(" + x + "," + y + ")";
    }

    public Point inverse() {
        return new Point(x * -1, y * -1);
    }

    public Point move(Direction direction) {
        Point offset = direction.getOffset();
        return new Point(x + offset.getX(), y + offset.getY());
    }

    public Point getOffset(Point point) {
        return new Point(point.getX() - getX(), point.getY() - getY());
    }

    @Override
    public int compareTo(Point o) {
        Integer x1 = x;
        Integer y1 = y;

        Integer x2 = o.getX();
        Integer y2 = o.getY();

        if (!x1.equals(x2)) {
            return x1.compareTo(x2);
        }

        return y1.compareTo(y2);
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof Point)) return false;
        final Point other = (Point) o;
        if (!other.canEqual(this)) return false;
        if (this.getX() != other.getX()) return false;
        return this.getY() == other.getY();
    }

    protected boolean canEqual(final Object other) {
        return other instanceof Point;
    }

    public int hashCode() {
        return x * y;
    }

    public int distance(Point pt) {
        int xDiff = Math.abs(getX() - pt.getX());
        int yDiff = Math.abs(getY() - pt.getY());

        int diagonals = Math.min(xDiff, yDiff);
        int cardinals = (xDiff - diagonals) + (yDiff - diagonals);
        return cardinals + diagonals;
    }

    public Point groundFloor() {
        return new Point(x, y % World.DISTANCE_BETWEEN_FLOORS);
    }
}
