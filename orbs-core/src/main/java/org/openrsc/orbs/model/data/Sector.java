package org.openrsc.orbs.model.data;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.openrsc.orbs.model.Point3D;
import org.openrsc.orbs.model.Tile;
import org.openrsc.orbs.model.TileValue;

@Builder(toBuilder = true)
@Getter
public class Sector implements Comparable<Sector> {
    private final Point3D location;

    @Setter
    private Tile[][] tiles;
    @Setter
    private TileValue[][] tileValues;

    public String getName() {
        return "h" + location.getZ() + "x" + location.getX() + "y" + location.getY();
    }


    // Match the zip file so that we go in order
    @Override
    public int compareTo(Sector other) {
        return location.compareTo(other.getLocation());
    }
}
