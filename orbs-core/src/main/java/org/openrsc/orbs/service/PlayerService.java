package org.openrsc.orbs.service;

import com.google.common.eventbus.EventBus;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import lombok.extern.log4j.Log4j2;
import org.openrsc.orbs.controller.dto.AppearanceDTO;
import org.openrsc.orbs.controller.dto.ConnectionStateDTO;
import org.openrsc.orbs.controller.dto.ManagedPlayerDTO;
import org.openrsc.orbs.database.PersistenceManager;
import org.openrsc.orbs.model.InventoryItem;
import org.openrsc.orbs.model.ItemDef;
import org.openrsc.orbs.model.TileTraversal;
import org.openrsc.orbs.model.TileTraversalProvider;
import org.openrsc.orbs.model.persisted.GroupDefinition;
import org.openrsc.orbs.model.persisted.PlayerDefinition;
import org.openrsc.orbs.net.builder.LoginPacketBuilder;
import org.openrsc.orbs.net.connection.ConnectionFactory;
import org.openrsc.orbs.net.connection.ConnectionHandler;
import org.openrsc.orbs.net.connection.player.PlayerConnection;
import org.openrsc.orbs.net.connection.player.script.Locations;
import org.openrsc.orbs.net.connection.player.script.Script;
import org.openrsc.orbs.net.connection.player.script.ScriptAPI;
import org.openrsc.orbs.net.opcode.OpcodeManager;
import org.openrsc.orbs.stateful.AbstractReducer;
import org.openrsc.orbs.stateful.bank.BankReducer;
import org.openrsc.orbs.stateful.config.ServerConfigurationReducer;
import org.openrsc.orbs.stateful.grounditems.GroundItemReducer;
import org.openrsc.orbs.stateful.inventory.InventoryReducer;
import org.openrsc.orbs.stateful.menu.MenuOptionReducer;
import org.openrsc.orbs.stateful.message.ServerMessageReducer;
import org.openrsc.orbs.stateful.objects.GameObjectsReducer;
import org.openrsc.orbs.stateful.player.LoginReducer;
import org.openrsc.orbs.stateful.player.NpcVisualReducer;
import org.openrsc.orbs.stateful.player.PlayerReducer;
import org.openrsc.orbs.stateful.player.PlayerState;
import org.openrsc.orbs.stateful.player.PlayerVisualReducer;
import org.openrsc.orbs.stateful.player.model.Player;
import org.openrsc.orbs.stateful.skills.StatsReducer;
import org.openrsc.orbs.stateful.sleep.FatigueReducer;
import org.openrsc.orbs.stateful.sleep.SleepReducer;
import org.openrsc.orbs.stateful.trade.TradeReducer;
import org.openrsc.orbs.util.World;
import org.openrsc.orbs.util.ocr.stormy.ocrlib.OCRException;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

import static org.openrsc.orbs.net.connection.player.ReconnectStrategy.NO_RETRY;

@Service
@Log4j2
public class PlayerService {
    private final Map<String, PlayerConnection> managedConnections = new ConcurrentHashMap<>();
    private final Map<String, Future<?>> playerFutures = new ConcurrentHashMap<>();
    private final ConnectionFactory connectionFactory;
    private final ExecutorService channelExecutorService;
    private final OpcodeManager opcodeManager;
    private final World world;
    private final ResourceLoader resourceLoader;
    private final PersistenceManager persistenceManager;

    public PlayerService(
            ConnectionFactory connectionFactory,
            OpcodeManager opcodeManager,
            World world,
            ResourceLoader resourceLoader,
            PersistenceManager persistenceManager
    ) {
        this.connectionFactory = connectionFactory;
        this.opcodeManager = opcodeManager;
        this.world = world;
        this.resourceLoader = resourceLoader;
        this.persistenceManager = persistenceManager;
        channelExecutorService = Executors.newFixedThreadPool(
                20,
                new ThreadFactoryBuilder().setNameFormat("ChannelThread-%d").build()
        );
        persistenceManager.getAllPlayers()
                .stream()
                .filter(PlayerDefinition::isActive)
                .map(PlayerDefinition::getUsername)
                .forEach(this::login);
    }

    public PlayerConnection getConnection(String username) {
        return managedConnections.get(username);
    }

    public PlayerState getPlayerState(String username) {
        if (!managedConnections.containsKey(username)) {
            throw new IllegalStateException("Requested player state for non-active connection.");
        }
        return managedConnections.get(username).getPlayerState();
    }

    public ScriptAPI getPlayerApi(String username) {
        final PlayerConnection connection = getConnection(username);
        if (connection != null) {
            return connection.getApi();
        }
        return null;
    }

    public Set<String> getPlayers() {
        return managedConnections.keySet();
    }

    public Set<GroupDefinition> getAllGroups() {
        return persistenceManager.getAllGroups();
    }

    public Set<PlayerDefinition> getAllPlayers() {
        return persistenceManager.getAllPlayers();
    }

    public void login(String username) {
        login(username, null);
    }

    public void login(String username, Script<?> script) {
        try {
            PlayerDefinition player = persistenceManager.getPlayerById(username);
            if (player != null) {
                PlayerState playerState = new PlayerState();
                playerState.setUsername(username);
                PlayerConnection playerConnection = initPlayerConnection(username, player.getPassword(), playerState);
                if (script != null) {
                    playerConnection.queueScript(script.getClass(), script.getParams());
                }
                managedConnections.put(username, playerConnection);
                Future<?> future = channelExecutorService.submit(playerConnection);
                playerFutures.put(username, future);
            }
        } catch (Exception ex) {
            log.error(
                    MessageFormat.format("Exception encountered for player {0}", username),
                    ex
            );
        }
    }

    public void logout(String username) {
        if (getPlayers().contains(username)) {
            PlayerConnection playerConnection = managedConnections.get(username);
            playerConnection.setExplicitlyRemoved(true);
            Future<?> future = playerFutures.get(username);
            if (future != null) {
                future.cancel(true);
            }
            Channel channel = playerConnection.getChannel();
            if (channel.isActive()) {
                channel.close();
            }
            managedConnections.remove(username);
            playerFutures.remove(username);
            System.gc();
        }
    }

    private PlayerConnection initPlayerConnection(
            String username,
            String password,
            PlayerState player
    ) throws InterruptedException, IOException, OCRException {
        ChannelFuture future = connectionFactory.connect("136.56.70.50", 43235);
//        ChannelFuture future = connectionFactory.connect("localhost", 43594);

        Channel channel = future.channel();
        EventBus playerEventBus = new EventBus(username + "-playerEventBus");
        EventBus scriptEventBus = new EventBus(username + "-scriptEventBus");
        channel.attr(ConnectionHandler.PLAYER_BUS_KEY).set(playerEventBus);
        channel.writeAndFlush(
                new LoginPacketBuilder().build(username, password)
        );

        Set<AbstractReducer> reducers = Set.of(
                new LoginReducer(player, playerEventBus, scriptEventBus),
                new PlayerReducer(player, playerEventBus),
                new PlayerVisualReducer(player, playerEventBus),
                new NpcVisualReducer(player.getNpcInfoByServerId(), playerEventBus),
                new GameObjectsReducer(player, player.getGameObjects(), playerEventBus),
                new StatsReducer(player.getSkills(), playerEventBus, scriptEventBus),
                new InventoryReducer(player.getInventory(), playerEventBus),
                new ServerMessageReducer(player, playerEventBus, scriptEventBus),
                new ServerConfigurationReducer(player, playerEventBus),
                new GroundItemReducer(player, playerEventBus),
                new FatigueReducer(player.getFatigue(), playerEventBus, scriptEventBus),
                new SleepReducer(player, playerEventBus, scriptEventBus, resourceLoader),
                new MenuOptionReducer(player, playerEventBus),
                new BankReducer(player.getBank(), playerEventBus),
                new TradeReducer(playerEventBus, player.getTrade())
        );

        return new PlayerConnection(
                username,
                password,
                player,
                channel,
                reducers,
                opcodeManager,
                world,
                scriptEventBus,
                this
        );
    }

    public TileTraversalProvider getTileProvider(String player) {
        return getTileProvider(getPlayerState(player));
    }

    public TileTraversalProvider getTileProvider(PlayerState playerState) {
        return point -> TileTraversal.combine(
                point,
                world.getTileTraversal(point),
                playerState.getObjectTraversalMap().computeIfAbsent(
                        point,
                        TileTraversal::new
                )
        );
    }

    public Collection<ManagedPlayerDTO> getManagedPlayers() {
        return managedConnections.keySet()
                .stream()
                .map(username -> {
                    try {
                        PlayerConnection connection = getConnection(username);
                        PlayerState state = getPlayerState(username);

                        ConnectionStateDTO connectionState = null;
                        if (connection.isDisconnected()) {
                            if (connection.getReconnectStrategy() == NO_RETRY) {
                                connectionState = ConnectionStateDTO.FATAL_ERROR;
                            } else {
                                connectionState = ConnectionStateDTO.RECONNECTING;
                            }
                        } else if (connection.getLoginResponse() == null) {
                            connectionState = ConnectionStateDTO.LOGGING_IN;
                        } else if (connection.getLoginResponse() != null) {
                            connectionState = ConnectionStateDTO.LOGGED_IN;
                        }

                        if (connectionState == ConnectionStateDTO.RECONNECTING
                                || connectionState == ConnectionStateDTO.LOGGING_IN
                                || connectionState == ConnectionStateDTO.FATAL_ERROR) {
                            return ManagedPlayerDTO.builder()
                                    .username(username)
                                    .connectionState(connectionState)
                                    .reconnectTimeTotal(connection.getRetryTime() - connection.getRetryTimeStarted())
                                    .reconnectTimeRemaining(connection.getRetryTime() - System.currentTimeMillis())
                                    .errorMessage(connection.getErrorMessage())
                                    .messageHistory(state.getMessageHistory().getMessages())
                                    .status(state.getStatus())
                                    .build();
                        }

                        Integer playerId = state.getPlayerId().get();
                        Player player = state.getPlayerInfoByPid().get(playerId);
                        List<Integer> wornItemAppearanceIds = player.getWornItemAppearanceIds();
                        int headType = 0;
                        int bodyType = 0;
                        if (wornItemAppearanceIds != null && wornItemAppearanceIds.size() > 2) {
                            headType = wornItemAppearanceIds.get(0);
                            bodyType = wornItemAppearanceIds.get(1);
                        }
                        final List<InventoryItem> inventoryById = state.getInventory().getBySlot();
                        Map<Integer, Integer> inventory = new TreeMap<>();
                        inventoryById.forEach(inventoryItem -> {
                            final ItemDef itemDef = inventoryItem.getDef();
                            if (itemDef != null) {
                                int itemId = itemDef.getId();
                                Integer currentCount = inventory.get(itemId);
                                if (currentCount == null) {
                                    inventory.put(itemId, inventoryItem.getAmount());
                                } else {
                                    inventory.put(itemId, currentCount + 1);
                                }
                            }
                        });
                        return ManagedPlayerDTO.builder()
                                .username(username)
                                .appearance(
                                        AppearanceDTO.builder()
                                                .head(headType)
                                                .body(bodyType)
                                                .hair(Byte.toUnsignedInt(player.getHairColor()))
                                                .top(Byte.toUnsignedInt(player.getTopColor()))
                                                .legs(Byte.toUnsignedInt(player.getPantsColor()))
                                                .skin(Byte.toUnsignedInt(player.getSkinColor()))
                                                .wornItemAppearanceIds(wornItemAppearanceIds)
                                                .build()
                                )
                                .status(state.getStatus())
                                .location(state.getLocation().toString())
                                .area(Locations.getNearest(state.getLocation()).getName())
                                .messageHistory(state.getMessageHistory().getMessages())
                                .inventory(inventory)
                                .connectionState(connectionState)
                                .reconnectTimeTotal(connection.getRetryTime() - connection.getRetryTimeStarted())
                                .reconnectTimeRemaining(connection.getRetryTime() - System.currentTimeMillis()).errorMessage(connection.getErrorMessage())
                                .scriptState(connection.getScriptState())
                                .scriptElapsedTime(connection.getCurrentScriptTimeElapsed())
                                .build();
                    } catch (Exception ignored) {

                    }
                    return null;
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(ManagedPlayerDTO::getUsername))));
    }
}
