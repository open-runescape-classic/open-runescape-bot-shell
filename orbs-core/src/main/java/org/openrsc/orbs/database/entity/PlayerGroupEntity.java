package org.openrsc.orbs.database.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;
import org.openrsc.orbs.model.persisted.GroupDefinition;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "PlayerGroup")
@Getter
@Setter
@FieldNameConstants
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class PlayerGroupEntity {
    @Id
    @EqualsAndHashCode.Include
    private String name;

    @ManyToMany(mappedBy = PlayerEntity.Fields.groups)
    private Set<PlayerEntity> members;

    public static GroupDefinition toGroupDefinition(PlayerGroupEntity entity) {
        return GroupDefinition.builder()
                .name(entity.getName())
                .players(
                        entity.getMembers().stream()
                                .map(PlayerEntity::toPlayerDefinition)
                                .collect(Collectors.toSet())
                )
                .build();
    }
}
