package org.openrsc.orbs.database.repository;

import org.openrsc.orbs.database.entity.PlayerGroupEntity;
import org.springframework.data.repository.CrudRepository;

public interface GroupRepository extends CrudRepository<PlayerGroupEntity, String> {
}
