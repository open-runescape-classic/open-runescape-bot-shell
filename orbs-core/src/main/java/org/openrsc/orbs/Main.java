package org.openrsc.orbs;

import org.openrsc.orbs.net.dto.server.config.ServerConfigurationEvent;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;
import java.text.MessageFormat;

@SpringBootApplication
public class Main {
    private static final String DATA_DIR = System.getProperty("user.home") + File.separator + "ORBS" + File.separator;

    public static ServerConfigurationEvent SERVER_CONFIGURATION;

    public static void main(String[] args) {
        File f = new File(DATA_DIR);
        if (!f.exists()) {
            try {
                if (!f.mkdir()) {
                    error();
                }
            } catch (Exception ex) {
                error();
            }
        }
        SpringApplication.run(Main.class, args);
    }

    public static void error() {
        throw new IllegalStateException(
                MessageFormat.format("Unable to create ORBS folder in home directory {0}", DATA_DIR)
        );
    }
}
