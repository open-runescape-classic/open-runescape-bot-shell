package org.openrsc.orbs.orbql.model;

public enum ExpressionType {
    COMPARISON,
    WRAPPED,
    NEGATION,
    FUNCTION_CALL,
    LITERAL
}
