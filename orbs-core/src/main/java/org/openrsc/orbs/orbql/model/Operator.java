package org.openrsc.orbs.orbql.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Set;

@AllArgsConstructor
@Getter
public enum Operator {
    EQ("="),
    NEQ("!="),
    LT("<"),
    LT_EQ("<="),
    GT(">"),
    GT_EQ(">="),
    OR("or"),
    AND("and");

    public static Set<Operator> NUMBER_OPERATORS = Set.of(LT, LT_EQ, GT, GT_EQ);
    private final String stringValue;

    public static Operator of(String value) {
        for (Operator operator : values()) {
            if (operator.getStringValue().equalsIgnoreCase(value)) {
                return operator;
            }
        }
        return null;
    }
}
