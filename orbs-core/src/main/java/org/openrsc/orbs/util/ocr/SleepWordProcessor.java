package org.openrsc.orbs.util.ocr;

import lombok.extern.log4j.Log4j2;
import org.apache.tomcat.util.security.MD5Encoder;
import org.openrsc.orbs.util.ocr.stormy.ocrlib.OCRException;
import org.springframework.core.io.ResourceLoader;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Scanner;

@Log4j2
public class SleepWordProcessor {
    private final Map<String, String> md5WordMap = new HashMap<>();

    public SleepWordProcessor(ResourceLoader resourceLoader) throws IOException, OCRException {
        Scanner scanner = new Scanner(resourceLoader.getResource("classpath:sleepwords.txt").getInputStream());
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            int colon = line.lastIndexOf(":");
            String md5 = line.substring(0, colon);
            String word = line.substring(colon + 1);
            md5WordMap.put(md5, word);
        }
    }

    public Optional<String> getSleepWord(byte[] data) {
        return Optional.ofNullable(md5WordMap.get(MD5Encoder.encode(data)));
    }
}
