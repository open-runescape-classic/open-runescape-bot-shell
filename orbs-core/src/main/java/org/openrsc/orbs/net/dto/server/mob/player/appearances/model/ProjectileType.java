package org.openrsc.orbs.net.dto.server.mob.player.appearances.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ProjectileType {
    MAGIC(1),
    RANGED(2);

    private final int type;

    public static ProjectileType byId(int id) {
        return id == 1 ? MAGIC : RANGED;
    }
}
