package org.openrsc.orbs.net.serializer;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.openrsc.orbs.net.annotation.model.DataType;

@AllArgsConstructor
@Getter
public class DataTypeConverter {
    private final Class<?> original;
    private final DataType protocolType;

    private static Number fromBoolean(Object b) {
        return (Boolean) b ? 1 : 0;
    }

    public Object toProtocolType(Object original) {
        Class<?> type = original.getClass();
        String simpleName = type.getSimpleName();
        switch (protocolType) {
            case BYTE:
                switch (simpleName) {
                    case "Boolean":
                    case "boolean":
                        return fromBoolean(original).byteValue();
                    case "Byte":
                    case "Short":
                    case "Integer":
                    case "Long":
                    case "byte":
                    case "short":
                    case "int":
                    case "long":
                        return ((Number) original).byteValue();
                }
                break;
            case UNSIGNED_SHORT:
                switch (simpleName) {
                    case "Boolean":
                    case "boolean":
                        return fromBoolean(original).shortValue();
                    case "Byte":
                    case "byte":
                        return ((Integer) Byte.toUnsignedInt((Byte) original)).shortValue();
                    case "Short":
                    case "Integer":
                    case "Long":
                    case "short":
                    case "int":
                    case "long":
                        return ((Number) original).shortValue();
                }
                break;
            case SHORT:
                switch (simpleName) {
                    case "Boolean":
                    case "boolean":
                        return fromBoolean(original).shortValue();
                    case "Byte":
                    case "Short":
                    case "Integer":
                    case "Long":
                    case "byte":
                    case "short":
                    case "int":
                    case "long":
                        return ((Number) original).shortValue();
                }
                break;
            case INT:
                switch (simpleName) {
                    case "Boolean":
                    case "boolean":
                        return fromBoolean(original).intValue();
                    case "Byte":
                    case "Short":
                    case "Integer":
                    case "Long":
                    case "byte":
                    case "short":
                    case "int":
                    case "long":
                        return ((Number) original).intValue();
                }
                break;
            case LONG:
                switch (simpleName) {
                    case "Boolean":
                    case "boolean":
                        return fromBoolean(original).longValue();
                    case "Byte":
                    case "Short":
                    case "Integer":
                    case "Long":
                    case "byte":
                    case "short":
                    case "int":
                    case "long":
                        return ((Number) original).longValue();
                }
                break;
        }
        return original;
    }
}
