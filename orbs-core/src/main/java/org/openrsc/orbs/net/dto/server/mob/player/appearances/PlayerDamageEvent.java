package org.openrsc.orbs.net.dto.server.mob.player.appearances;

import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.openrsc.orbs.net.dto.server.mob.player.appearances.model.PlayerUpdateType;

@SuperBuilder
@Getter
public class PlayerDamageEvent extends PlayerHpEvent {
    private final PlayerUpdateType type = PlayerUpdateType.DAMAGE_UPDATE;
    private final int damageTaken;
}
