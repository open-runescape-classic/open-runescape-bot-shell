package org.openrsc.orbs.net.dto.server.trade;

import lombok.Getter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.openrsc.orbs.net.annotation.PacketSerializable;
import org.openrsc.orbs.net.annotation.RegisterEventOpcode;
import org.openrsc.orbs.net.dto.server.Event;
import org.openrsc.orbs.net.opcode.ServerOpcode;

@SuperBuilder
@Getter
@PacketSerializable
@RegisterEventOpcode(ServerOpcode.SEND_TRADE_CLOSE)
@ToString
public class TradeCloseWindowEvent extends Event {
}
