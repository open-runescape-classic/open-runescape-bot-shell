package org.openrsc.orbs.net.dto;

import lombok.Getter;
import org.openrsc.orbs.net.annotation.PacketSerializable;

@Getter
@PacketSerializable
public enum Spell {
    WIND_STRIKE(0),
    CONFUSE(1),
    WATER_STRIKE(2),
    ENCHANT_LVL1_AMULET(3),
    EARTH_STRIKE(4),
    WEAKEN(5),
    FIRE_STRIKE(6),
    BONES_TO_BANANAS(7),
    WIND_BOLT(8),
    CURSE(9),
    LOW_LEVEL_ALCHEMY(10),
    WATER_BOLT(11),
    VARROCK_TELEPORT(12),
    ENCHANT_LVL2_AMULET(13),
    EARTH_BOLT(14),
    LUMBRIDGE_TELEPORT(15),
    TELEKINETIC_GRAB(16),
    FIRE_BOLT(17),
    FALADOR_TELEPORT(18),
    CRUMBLE_UNDEAD(19),
    WIND_BLAST(20),
    SUPERHEAT_ITEM(21),
    CAMELOT_TELEPORT(22),
    WATER_BLAST(23),
    ENCHANT_LVL3_AMULET(24),
    IBAN_BLAST(25),
    ARDOUGNE_TELEPORT(26),
    EARTH_BLAST(27),
    HIGH_LEVEL_ALCHEMY(28),
    CHARGE_WATER_ORB(29),
    ENCHANT_LVL4_AMULET(30),
    WATCHTOWER_TELEPORT(31),
    FIRE_BLAST(32),
    CHARGE_EARTH_ORB(33),
    CLAWS_OF_GUTHIX(34),
    SARADOMIN_STRIKE(35),
    FLAMES_OF_ZAMORAK(36),
    WIND_WAVE(37),
    CHARGE_FIRE_ORB(38),
    WATER_WAVE(39),
    CHARGE_AIR_ORB(40),
    VULNERABILITY(41),
    ENCHANT_LVL5_AMULET(42),
    EARTH_WAVE(43),
    ENFEEBLE(44),
    FIRE_WAVE(45),
    STUN(46),
    CHARGE(47);

    private final short id;

    Spell(int id) {
        this.id = (short) id;
    }
}
