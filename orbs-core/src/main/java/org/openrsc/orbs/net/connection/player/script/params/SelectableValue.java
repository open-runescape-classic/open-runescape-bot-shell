package org.openrsc.orbs.net.connection.player.script.params;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Getter
public class SelectableValue<L> {
    private final String label;
    private final L value;

    public static <T extends Enum<T>> List<SelectableValue<T>> of(T[] values) {
        return Arrays.stream(values)
                .map(SelectableValue::of)
                .collect(Collectors.toList());
    }

    private static <T extends Enum<T>> SelectableValue<T> of(T enumValue) {
        return new SelectableValue<>(enumValue.toString(), enumValue);
    }
}
