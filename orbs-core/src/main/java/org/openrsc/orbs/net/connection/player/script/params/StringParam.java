package org.openrsc.orbs.net.connection.player.script.params;

import lombok.Getter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
public class StringParam extends ScriptParam {
    @Override
    public ScriptParamType getType() {
        return ScriptParamType.STRING;
    }
}
