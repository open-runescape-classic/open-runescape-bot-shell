package org.openrsc.orbs.net.connection.player.script.model;

import lombok.Getter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
public class SlottedItem extends Item {
    private final int slotId;
}
