package org.openrsc.orbs.net.dto.server.trade;

import lombok.Getter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.openrsc.orbs.net.annotation.PacketSerializable;
import org.openrsc.orbs.net.annotation.RegisterEventOpcode;
import org.openrsc.orbs.net.annotation.TypeOverride;
import org.openrsc.orbs.net.annotation.model.DataType;
import org.openrsc.orbs.net.dto.server.Event;
import org.openrsc.orbs.net.opcode.ServerOpcode;

@SuperBuilder
@Getter
@PacketSerializable
@RegisterEventOpcode(ServerOpcode.SEND_TRADE_ACCEPTED)
@ToString
public class SelfAcceptedTradeEvent extends Event {
    @TypeOverride(DataType.BYTE)
    private final boolean accepted;
}
