package org.openrsc.orbs.net.dto.client.item;

import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.openrsc.orbs.net.annotation.PacketSerializable;
import org.openrsc.orbs.net.annotation.RegisterActionOpcode;
import org.openrsc.orbs.net.annotation.TypeOverride;
import org.openrsc.orbs.net.annotation.model.DataType;
import org.openrsc.orbs.net.dto.Point;
import org.openrsc.orbs.net.dto.client.Action;
import org.openrsc.orbs.net.opcode.ClientOpcode;

@SuperBuilder
@Getter
@PacketSerializable
@RegisterActionOpcode(ClientOpcode.GROUND_ITEM_USE_ITEM)
public class ItemOnGroundItemAction extends Action {
    private final Point location;
    @TypeOverride(DataType.SHORT)
    private final int slotIndex;
    @TypeOverride(DataType.SHORT)
    private final int groundItemId;
}
