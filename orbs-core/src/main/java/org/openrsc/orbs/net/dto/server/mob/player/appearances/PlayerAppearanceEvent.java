package org.openrsc.orbs.net.dto.server.mob.player.appearances;

import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.openrsc.orbs.net.dto.server.mob.player.appearances.model.PlayerIcon;
import org.openrsc.orbs.net.dto.server.mob.player.appearances.model.PlayerUpdateType;

import java.util.List;

@SuperBuilder
@Getter
public class PlayerAppearanceEvent extends PlayerUpdateEvent {
    private final PlayerUpdateType type = PlayerUpdateType.PLAYER_APPEARANCE_IDENTITY;
    private final String username;
    private final List<Integer> wornItemAppearanceIds;
    private final byte headType;
    private final byte bodyType;
    private final byte hairColor;
    private final byte topColor;
    private final byte pantsColor;
    private final byte skinColor;
    private final int combatLevel;
    private final boolean skulled;
    private final String clanTag;
    private final boolean invisible;
    private final boolean invulnerable;
    private final int groupId;
    private final PlayerIcon icon;
}