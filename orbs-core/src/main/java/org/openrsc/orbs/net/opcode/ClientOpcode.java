package org.openrsc.orbs.net.opcode;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

// This is from the server's perspective (Server <-- OpcodeIn)
@AllArgsConstructor
@Getter
public enum ClientOpcode {
    HEARTBEAT(67),
    WALK_TO_ENTITY(16),
    WALK_TO_POINT(187),
    CONFIRM_LOGOUT(31),
    LOGOUT(102),
    BLINK(59),
    COMBAT_STYLE_CHANGED(29),
    QUESTION_DIALOG_ANSWER(116),
    REQUEST_SERVER_CONFIG(19),

    PLAYER_APPEARANCE_CHANGE(235),
    SOCIAL_ADD_IGNORE(132),
    SOCIAL_ADD_DELAYED_IGNORE(194),
    SOCIAL_ADD_FRIEND(195),
    SOCIAL_SEND_PRIVATE_MESSAGE(218),
    SOCIAL_REMOVE_FRIEND(167),
    SOCIAL_REMOVE_IGNORE(241),

    DUEL_FIRST_SETTINGS_CHANGED(8),
    DUEL_FIRST_ACCEPTED(176),
    DUEL_DECLINED(197),
    DUEL_OFFER_ITEM(33),
    DUEL_SECOND_ACCEPTED(77),

    INTERACT_WITH_BOUNDARY(14),
    INTERACT_WITH_BOUNDARY2(127),
    CAST_ON_BOUNDARY(180),
    USE_WITH_BOUNDARY(161),

    NPC_TALK_TO(153),
    NPC_COMMAND(202),
    NPC_COMMAND2(203),
    NPC_ATTACK(190),
    CAST_ON_NPC(50),
    NPC_USE_ITEM(135),

    PLAYER_CAST_PVP(229),
    PLAYER_USE_ITEM(113),
    PLAYER_ATTACK(171),
    PLAYER_DUEL(103),
    PLAYER_INIT_TRADE_REQUEST(142),
    PLAYER_FOLLOW(165),

    CAST_ON_GROUND_ITEM(249),
    GROUND_ITEM_USE_ITEM(53),
    GROUND_ITEM_TAKE(247),

    CAST_ON_INVENTORY_ITEM(4),
    ITEM_USE_ITEM(91),
    ITEM_UNEQUIP_FROM_INVENTORY(170),
    ITEM_EQUIP_FROM_INVENTORY(169),
    ITEM_UNEQUIP_FROM_EQUIPMENT(168),
    ITEM_EQUIP_FROM_BANK(172),
    ITEM_REMOVE_TO_BANK(173),
    ITEM_COMMAND(90),
    ITEM_DROP(246),

    CAST_ON_SELF(137),
    CAST_ON_LAND(158),

    OBJECT_COMMAND(136),
    OBJECT_COMMAND2(79),
    CAST_ON_SCENERY(99),
    USE_ITEM_ON_SCENERY(115),

    SHOP_CLOSE(166),
    SHOP_BUY(236),
    SHOP_SELL(221),

    PLAYER_ACCEPTED_INIT_TRADE_REQUEST(55),
    PLAYER_DECLINED_TRADE(230),
    PLAYER_ADDED_ITEMS_TO_TRADE_OFFER(46),
    PLAYER_ACCEPTED_TRADE(104),

    PRAYER_ACTIVATED(60),
    PRAYER_DEACTIVATED(254),

    GAME_SETTINGS_CHANGED(111),
    CHAT_MESSAGE(216),
    COMMAND(38),
    PRIVACY_SETTINGS_CHANGED(64),
    REPORT_ABUSE(206),

    BANK_CLOSE(212),
    BANK_WITHDRAW(22),
    BANK_DEPOSIT(23),
    BANK_DEPOSIT_ALL_FROM_INVENTORY(24),
    BANK_DEPOSIT_ALL_FROM_EQUIPMENT(26),
    BANK_SAVE_PRESET(27),
    BANK_LOAD_PRESET(28),

    INTERFACE_OPTIONS(199),
    SLEEP_WORD_ENTERED(45),
    SKIP_TUTORIAL(84),

    ON_BLACK_HOLE(86),
    NPC_DEFINITION_REQUEST(89),
    LOGIN(0),

    REGISTER_ACCOUNT(2),
    CHANGE_PASS(25),
    SET_RECOVERY(208),
    SET_DETAILS(253),
    CANCEL_RECOVERY_REQUEST(196);

    private static final Map<Integer, ClientOpcode> opcodeMap;

    static {
        opcodeMap = new HashMap<>();
        Arrays.stream(ClientOpcode.values())
                .forEach(ClientOpcode::addToMap);
    }

    private final int opCode;

    private static void addToMap(ClientOpcode clientOpcode) {
        opcodeMap.put(clientOpcode.getOpCode(), clientOpcode);
    }

    public static ClientOpcode fromOpcode(int id) {
        return opcodeMap.get(id);
    }
}
