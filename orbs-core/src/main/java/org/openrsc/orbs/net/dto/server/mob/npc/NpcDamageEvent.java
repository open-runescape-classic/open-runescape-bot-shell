package org.openrsc.orbs.net.dto.server.mob.npc;

import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.openrsc.orbs.net.dto.server.mob.npc.model.NpcUpdateType;

@SuperBuilder
@Getter
public class NpcDamageEvent extends NpcUpdateEvent {
    private final NpcUpdateType type = NpcUpdateType.DAMAGE_UPDATE;
    private final int damage;
    private final int currentHp;
    private final int maxHp;
}
