package org.openrsc.orbs.net.serializer.custom;

import com.tomgibara.bits.BitReader;
import org.openrsc.orbs.net.dto.server.mob.npc.NpcCoordsEvent;
import org.openrsc.orbs.net.dto.server.mob.npc.NpcPositionUpdate;
import org.openrsc.orbs.net.model.Packet;
import org.openrsc.orbs.net.serializer.PacketDeserializer;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Set;

public class NpcCoordsEventDeserializer implements PacketDeserializer<NpcCoordsEvent> {
    @Override
    public NpcCoordsEvent deserialize(Packet packet, Class<?> type) {
        int remainingBits = packet.getReadableBytes() * 8;
        BitReader reader = packet.bitReader();
        Set<Integer> removedNpcs = getRemovedNpcIndices(reader);

        LinkedHashMap<Integer, NpcPositionUpdate> npcPositionUpdates = new LinkedHashMap<>();
        remainingBits -= reader.getPosition();
        int iterations = remainingBits / 38;
        for (int i = 0; i < iterations; i++) {
            int serverId = reader.read(12);
            int offsetX = reader.read(6);
            int offsetY = reader.read(6);
            // Ignore sprite
            reader.read(4);
            int npcId = reader.read(10);

            npcPositionUpdates.put(
                    serverId,
                    NpcPositionUpdate.builder()
                            .offsetX(offsetX)
                            .offsetY(offsetY)
                            .npcId(npcId)
                            .build()
            );
        }

        return NpcCoordsEvent.builder()
                .removedNpcs(removedNpcs)
                .npcStatusUpdates(npcPositionUpdates)
                .build();
    }

    private Set<Integer> getRemovedNpcIndices(BitReader reader) {
        int localNpcCount = reader.read(8);
        Set<Integer> removedNpcIndices = new HashSet<>();
        for (int i = 0; i < localNpcCount; i++) {
            boolean needsUpdate = reader.readBoolean();
            if (needsUpdate) {
                boolean isSpriteUpdate = reader.readBoolean();
                // Sprite changed or should remove (and re-add, potentially)
                if (isSpriteUpdate) {
                    long markedPosition = reader.getPosition();
                    boolean shouldRemove = reader.read(2) == 3;
                    if (shouldRemove) {
                        removedNpcIndices.add(i);
                    } else {
                        reader.setPosition(markedPosition);
                        // sprite change, ignore
                        reader.read(4);
                    }
                } else {
                    reader.read(3);
                }
            }
        }
        return removedNpcIndices;
    }
}
