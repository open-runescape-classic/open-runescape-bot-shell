package org.openrsc.orbs.net.dto.client.shop;

import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.openrsc.orbs.net.annotation.PacketSerializable;
import org.openrsc.orbs.net.annotation.RegisterActionOpcode;
import org.openrsc.orbs.net.annotation.TypeOverride;
import org.openrsc.orbs.net.annotation.model.DataType;
import org.openrsc.orbs.net.dto.client.Action;
import org.openrsc.orbs.net.opcode.ClientOpcode;

@SuperBuilder
@Getter
@PacketSerializable
@RegisterActionOpcode(ClientOpcode.SHOP_BUY)
public class BuyShopItemAction extends Action {
    @TypeOverride(DataType.SHORT)
    private final int catalogId;
    @TypeOverride(DataType.UNSIGNED_SHORT)
    private final int stockAmount;
    @TypeOverride(DataType.UNSIGNED_SHORT)
    private final int amount;
}
