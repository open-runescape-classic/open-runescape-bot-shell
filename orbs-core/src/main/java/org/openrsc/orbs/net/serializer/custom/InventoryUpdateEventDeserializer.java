package org.openrsc.orbs.net.serializer.custom;

import org.openrsc.orbs.model.InventoryItem;
import org.openrsc.orbs.model.ItemDef;
import org.openrsc.orbs.net.dto.server.inventory.InventoryUpdateEvent;
import org.openrsc.orbs.net.model.Packet;
import org.openrsc.orbs.net.serializer.PacketDeserializer;
import org.openrsc.orbs.util.EntityService;

public class InventoryUpdateEventDeserializer implements PacketDeserializer<InventoryUpdateEvent> {
    public static final int ITEM_ID_WIELDED_OFFSET = 32768;

    @Override
    public InventoryUpdateEvent deserialize(Packet packet, Class<?> type) {
        int slotId = Byte.toUnsignedInt(packet.readByte());
        int itemId = Short.toUnsignedInt(packet.readShort());
        boolean wielded = false;
        if (itemId >= ITEM_ID_WIELDED_OFFSET) {
            wielded = true;
            itemId -= ITEM_ID_WIELDED_OFFSET;
        }
        boolean noted = (int) packet.readByte() != 0;
        ItemDef def = EntityService.getItemDef(itemId);
        int amount = 1;
        if (def.isStackable() || noted) {
            amount = packet.readInt();
        }

        return InventoryUpdateEvent.builder()
                .slotId(slotId)
                .item(
                        InventoryItem.builder()
                                .def(def)
                                .wielded(wielded)
                                .noted(noted)
                                .amount(amount)
                                .build()
                )
                .build();
    }
}
