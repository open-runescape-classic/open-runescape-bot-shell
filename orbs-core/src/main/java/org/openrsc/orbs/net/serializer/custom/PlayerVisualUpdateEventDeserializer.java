package org.openrsc.orbs.net.serializer.custom;

import lombok.extern.log4j.Log4j2;
import org.openrsc.orbs.net.dto.server.mob.player.PlayerVisualUpdateEvent;
import org.openrsc.orbs.net.dto.server.mob.player.appearances.PlayerAppearanceEvent;
import org.openrsc.orbs.net.dto.server.mob.player.appearances.PlayerBubbleEvent;
import org.openrsc.orbs.net.dto.server.mob.player.appearances.PlayerDamageEvent;
import org.openrsc.orbs.net.dto.server.mob.player.appearances.PlayerHpEvent;
import org.openrsc.orbs.net.dto.server.mob.player.appearances.PlayerOnNpcProjectileEvent;
import org.openrsc.orbs.net.dto.server.mob.player.appearances.PlayerOnPlayerProjectileEvent;
import org.openrsc.orbs.net.dto.server.mob.player.appearances.PlayerPublicChatEvent;
import org.openrsc.orbs.net.dto.server.mob.player.appearances.PlayerQuestChatEvent;
import org.openrsc.orbs.net.dto.server.mob.player.appearances.model.PlayerIcon;
import org.openrsc.orbs.net.dto.server.mob.player.appearances.model.PlayerUpdateType;
import org.openrsc.orbs.net.dto.server.mob.player.appearances.model.ProjectileType;
import org.openrsc.orbs.net.model.Packet;
import org.openrsc.orbs.net.serializer.PacketDeserializer;

import java.util.ArrayList;
import java.util.List;

@Log4j2
public class PlayerVisualUpdateEventDeserializer implements PacketDeserializer<PlayerVisualUpdateEvent> {
    @Override
    public PlayerVisualUpdateEvent deserialize(Packet packet, Class<?> type) {
        PlayerVisualUpdateEvent.PlayerVisualUpdateEventBuilder builder = PlayerVisualUpdateEvent.builder();

        short numberOfUpdates = packet.readShort();
        for (int i = 0; i < numberOfUpdates; i++) {
            int playerId = packet.readShort();
            int updateTypeInt = Byte.toUnsignedInt(packet.readByte());
            PlayerUpdateType updateType = PlayerUpdateType.byId(updateTypeInt);
            if (updateType == null) {
                log.info("Unknown update type: " + updateTypeInt);
                continue;
            }
            switch (updateType) {
                case BUBBLE -> {
                    int itemId = packet.readShort();
                    builder.updateEvent(
                            PlayerBubbleEvent.builder()
                                    .playerId(playerId)
                                    .itemId(itemId)
                                    .build()
                    );
                }
                case PUBLIC_CHAT -> {
                    PlayerIcon icon = getPlayerIcon(packet.readInt());
                    String message = packet.readString();
                    builder.updateEvent(
                            PlayerPublicChatEvent.builder()
                                    .playerId(playerId)
                                    .icon(icon)
                                    .message(message)
                                    .build()
                    );
                }
                case DAMAGE_UPDATE -> {
                    int damage = Byte.toUnsignedInt(packet.readByte());
                    int currentHp = Byte.toUnsignedInt(packet.readByte());
                    int maxHp = Byte.toUnsignedInt(packet.readByte());
                    builder.updateEvent(
                            PlayerDamageEvent.builder()
                                    .playerId(playerId)
                                    .damageTaken(damage)
                                    .currentHp(currentHp)
                                    .maxHp(maxHp)
                                    .build()
                    );
                }
                case PROJECTILE_PLAYER -> {
                    ProjectileType projectileType = ProjectileType.byId(packet.readShort());
                    int victimPid = packet.readShort();
                    builder.updateEvent(
                            PlayerOnPlayerProjectileEvent.builder()
                                    .playerId(playerId)
                                    .projectileType(projectileType)
                                    .victimPid(victimPid)
                                    .build()
                    );
                }
                case PROJECTILE_NPC -> {
                    ProjectileType projectileType1 = ProjectileType.byId(packet.readShort());
                    int victimServerIndex = packet.readShort();
                    builder.updateEvent(
                            PlayerOnNpcProjectileEvent.builder()
                                    .playerId(playerId)
                                    .projectileType(projectileType1)
                                    .victimServerIndex(victimServerIndex)
                                    .build()
                    );
                }
                case PLAYER_APPEARANCE_IDENTITY -> {
                    String username = packet.readString();
                    int wornItemsLength = Byte.toUnsignedInt(packet.readByte());
                    List<Integer> wornItems = new ArrayList<>(wornItemsLength);
                    for (int j = 0; j < wornItemsLength; j++) {
                        wornItems.add(Short.toUnsignedInt(packet.readShort()));
                    }
                    byte hairColor = packet.readByte();
                    byte topColor = packet.readByte();
                    byte pantsColor = packet.readByte();
                    byte skinColor = packet.readByte();
                    int combatLevel = Byte.toUnsignedInt(packet.readByte());
                    boolean skulled = packet.readBoolean();
                    boolean hasClan = packet.readBoolean();
                    String clanTag = null;
                    if (hasClan) {
                        clanTag = packet.readString();
                    }
                    boolean invisible = packet.readBoolean();
                    boolean invulnerable = packet.readBoolean();
                    int groupId = Byte.toUnsignedInt(packet.readByte());
                    PlayerIcon icon = getPlayerIcon(packet.readInt());
                    builder.updateEvent(
                            PlayerAppearanceEvent.builder()
                                    .username(username)
                                    .playerId(playerId)
                                    .wornItemAppearanceIds(wornItems)
                                    .hairColor(hairColor)
                                    .topColor(topColor)
                                    .pantsColor(pantsColor)
                                    .skinColor(skinColor)
                                    .combatLevel(combatLevel)
                                    .skulled(skulled)
                                    .clanTag(clanTag)
                                    .invisible(invisible)
                                    .invulnerable(invulnerable)
                                    .groupId(groupId)
                                    .icon(icon)
                                    .build()
                    );
                }
                case QUEST_CHAT -> {
                    String msg = packet.readString();
                    builder.updateEvent(
                            PlayerQuestChatEvent.builder()
                                    .playerId(playerId)
                                    .message(msg)
                                    .build()
                    );
                }
                case MUTED_CHAT -> {
                    PlayerIcon icon = getPlayerIcon(packet.readInt());
                    boolean muted = packet.readBoolean();
                    boolean tutorialIslandPlayer = packet.readBoolean();
                    String msg = packet.readString();
                }
                case HP_UPDATE -> builder.updateEvent(
                        PlayerHpEvent.builder()
                                .playerId(playerId)
                                .currentHp(Byte.toUnsignedInt(packet.readByte()))
                                .maxHp(Byte.toUnsignedInt(packet.readByte()))
                                .build()
                );
            }
        }

        return builder.build();
    }

    private PlayerIcon getPlayerIcon(int icon) {
        return PlayerIcon.byId(icon)
                .stream()
                .findFirst()
                .orElse(null);
    }
}
