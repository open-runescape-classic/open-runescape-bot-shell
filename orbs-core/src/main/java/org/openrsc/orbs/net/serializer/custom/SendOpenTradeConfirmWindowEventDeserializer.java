package org.openrsc.orbs.net.serializer.custom;

import org.openrsc.orbs.net.dto.server.trade.SendOpenTradeConfirmWindowEvent;
import org.openrsc.orbs.net.model.Packet;
import org.openrsc.orbs.net.serializer.PacketDeserializer;

public class SendOpenTradeConfirmWindowEventDeserializer implements PacketDeserializer<SendOpenTradeConfirmWindowEvent> {
    @Override
    public SendOpenTradeConfirmWindowEvent deserialize(Packet packet, Class<?> object) {
        final SendOpenTradeConfirmWindowEvent.SendOpenTradeConfirmWindowEventBuilder<?, ?> builder = SendOpenTradeConfirmWindowEvent.builder();
        builder.otherPlayer(packet.readString());
        builder.receivedItems(TradeOfferUtils.readTradeItems(packet));
        builder.offeredItems(TradeOfferUtils.readTradeItems(packet));
        return builder.build();
    }
}
