package org.openrsc.orbs.net.serializer.custom;

import com.tomgibara.bits.BitReader;
import lombok.extern.log4j.Log4j2;
import org.openrsc.orbs.net.dto.Point;
import org.openrsc.orbs.net.dto.server.mob.player.PlayerCoordsEvent;
import org.openrsc.orbs.net.model.Packet;
import org.openrsc.orbs.net.serializer.PacketDeserializer;
import org.openrsc.orbs.net.util.DataConversions;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Set;

@Log4j2
public class PlayerCoordsEventDeserializer implements PacketDeserializer<PlayerCoordsEvent> {
    @Override
    public PlayerCoordsEvent deserialize(Packet packet, Class<?> type) {
        int remainingBits = packet.getReadableBytes() * 8;
        BitReader reader = packet.bitReader();
        Point currentPlayerLocation = new Point(reader.read(11), reader.read(13));
        int spriteUpdate = reader.read(4);
        boolean inCombat = spriteUpdate == 9 || spriteUpdate == 8;
        // this is truncated at 256? It iterates over the full list regardless of this number
        int localPlayerCount = reader.read(8);

        Set<Integer> removedPlayers = getRemovedPlayers(reader, localPlayerCount);

        LinkedHashMap<Integer, Point> playerStatusUpdates = new LinkedHashMap<>();
        LinkedHashMap<Integer, Boolean> playersInCombat = new LinkedHashMap<>();
        remainingBits -= reader.getPosition();
        // calculate pid,offsetx,offsety,sprite updates
        int playerUpdates = remainingBits / 27;
        for (int i = 0; i < playerUpdates; i++) {
            int playerId = reader.read(11);
            int offsetX = reader.read(6);
            int offsetY = reader.read(6);
            Point playerLocation = DataConversions.reverseCoordOffsets(offsetX, offsetY, currentPlayerLocation);
            // ignore sprite
            int sprite = reader.read(4);
            boolean playerInCombat = sprite == 9 || sprite == 8;
            playerStatusUpdates.put(playerId, playerLocation);
            playersInCombat.put(playerId, playerInCombat);
        }

        return PlayerCoordsEvent.builder()
                .currentPlayerLocation(currentPlayerLocation)
                .removedPlayers(removedPlayers)
                .playerStatusUpdates(playerStatusUpdates)
                .playersInCombat(playersInCombat)
                .inCombat(inCombat)
                .build();
    }

    private Set<Integer> getRemovedPlayers(BitReader reader, int localPlayerCount) {
        Set<Integer> removedPlayers = new HashSet<>();
        for (int i = 0; i < localPlayerCount; i++) {
            boolean needsUpdate = reader.readBoolean();
            if (needsUpdate) {
                boolean isSpriteUpdate = reader.readBoolean();
                // Sprite changed or should remove (and re-add, potentially)
                if (isSpriteUpdate) {
                    long markedPosition = reader.getPosition();
                    boolean shouldRemove = reader.read(2) == 3;
                    if (shouldRemove) {
                        removedPlayers.add(i);
                    } else {
                        reader.setPosition(markedPosition);
                        // sprite change, ignore
                        reader.read(4);
                    }
                }
            }
        }
        return removedPlayers;
    }
}
