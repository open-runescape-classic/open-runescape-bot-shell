package org.openrsc.orbs.net.dto.server.mob.npc;

import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.openrsc.orbs.net.dto.server.mob.npc.model.NpcUpdateType;

@SuperBuilder
@Getter
public class NpcChatEvent extends NpcUpdateEvent {
    private final NpcUpdateType type = NpcUpdateType.CHAT_MESSAGE;
    private final Integer recipientPlayerId;
    private final String message;
}