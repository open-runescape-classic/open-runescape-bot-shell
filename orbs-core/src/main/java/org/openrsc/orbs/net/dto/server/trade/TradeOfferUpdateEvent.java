package org.openrsc.orbs.net.dto.server.trade;

import lombok.Getter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.openrsc.orbs.net.annotation.PacketSerializable;
import org.openrsc.orbs.net.annotation.RegisterEventOpcode;
import org.openrsc.orbs.net.connection.player.script.model.Item;
import org.openrsc.orbs.net.dto.server.Event;
import org.openrsc.orbs.net.opcode.ServerOpcode;
import org.openrsc.orbs.net.serializer.custom.TradeOfferUpdateEventDeserializer;

import java.util.List;

@SuperBuilder
@Getter
@PacketSerializable(deserializer = TradeOfferUpdateEventDeserializer.class)
@RegisterEventOpcode(ServerOpcode.SEND_TRADE_OTHER_ITEMS)
@ToString
public class TradeOfferUpdateEvent extends Event {
    private final List<Item> receivedItems;
    private final List<Item> offeredItems;
}
