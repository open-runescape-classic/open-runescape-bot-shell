package org.openrsc.orbs.net.dto.server.inventory;

import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.openrsc.orbs.model.InventoryItem;
import org.openrsc.orbs.net.annotation.PacketSerializable;
import org.openrsc.orbs.net.annotation.RegisterEventOpcode;
import org.openrsc.orbs.net.dto.server.Event;
import org.openrsc.orbs.net.opcode.ServerOpcode;
import org.openrsc.orbs.net.serializer.custom.SendInventoryEventDeserializer;

import java.util.List;

@SuperBuilder
@Getter
@PacketSerializable(deserializer = SendInventoryEventDeserializer.class)
@RegisterEventOpcode(ServerOpcode.SEND_INVENTORY)
public class SendInventoryEvent extends Event {
    private final List<InventoryItem> inventoryItems;
}
