package org.openrsc.orbs.net.serializer.custom;

import org.openrsc.orbs.net.dto.server.message.MessageType;
import org.openrsc.orbs.net.dto.server.message.ServerMessageEvent;
import org.openrsc.orbs.net.dto.server.mob.player.appearances.model.PlayerIcon;
import org.openrsc.orbs.net.model.Packet;
import org.openrsc.orbs.net.serializer.PacketDeserializer;

public class ServerMessageEventDeserializer implements PacketDeserializer<ServerMessageEvent> {
    @Override
    public ServerMessageEvent deserialize(Packet packet, Class<?> type) {
        ServerMessageEvent.ServerMessageEventBuilder builder = ServerMessageEvent.builder();

        PlayerIcon icon = PlayerIcon.firstById(packet.readInt());
        MessageType messageType = MessageType.byId(Byte.toUnsignedInt(packet.readByte()));
        byte infoContained = packet.readByte();
        boolean hasSender = (infoContained & 1) != 0;
        boolean hasColor = (infoContained & 2) != 0;

        String message = packet.readString();
        String sender = null;
        String colorString = null;
        if (hasSender) {
            sender = packet.readString();
            // sends sender twice for some reason
            packet.readString();
        }

        if (hasColor) {
            colorString = packet.readString();
        }

        return builder
                .messageType(messageType)
                .icon(icon)
                .message(message)
                .sender(sender)
                .colorString(colorString)
                .build();
    }
}
