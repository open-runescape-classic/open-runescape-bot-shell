package org.openrsc.orbs.net.connection.player.script.model;

import lombok.Getter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
@ToString
public class Item {
    private final int id;
    private final int amount;
    private final boolean noted;
}
