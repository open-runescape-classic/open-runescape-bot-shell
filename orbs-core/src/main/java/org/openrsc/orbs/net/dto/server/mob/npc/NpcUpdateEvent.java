package org.openrsc.orbs.net.dto.server.mob.npc;

import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.openrsc.orbs.net.dto.server.Event;
import org.openrsc.orbs.net.dto.server.mob.npc.model.NpcUpdateType;

@SuperBuilder
@Getter
public class NpcUpdateEvent extends Event {
    private final int serverIndex;
    private final NpcUpdateType type;
}
