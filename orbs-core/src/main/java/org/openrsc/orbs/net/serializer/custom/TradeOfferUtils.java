package org.openrsc.orbs.net.serializer.custom;

import org.openrsc.orbs.net.connection.player.script.model.Item;
import org.openrsc.orbs.net.model.Packet;

import java.util.ArrayList;
import java.util.List;

public class TradeOfferUtils {
    public static List<Item> readTradeItems(Packet packet) {
        List<Item> tradeItems = new ArrayList<>();
        int count = packet.readByte();
        for (int i = 0; i < count; i++) {
            int itemId = Short.toUnsignedInt(packet.readShort());
            int amount = packet.readInt();
            tradeItems.add(
                    Item.builder()
                            .id(itemId)
                            .amount(amount)
                            .noted(false)
                            .build()
            );
        }
        return tradeItems;
    }
}