package org.openrsc.orbs.net.dto.server.mob.npc;

import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.openrsc.orbs.net.dto.server.mob.npc.model.NpcUpdateType;
import org.openrsc.orbs.net.dto.server.mob.player.appearances.model.ProjectileType;

@SuperBuilder
@Getter
public class NpcOnNpcProjectileEvent extends NpcUpdateEvent {
    private final NpcUpdateType type = NpcUpdateType.PROJECTILE_NPC;
    private final ProjectileType projectileType;
    private final int victimServerIndex;
}