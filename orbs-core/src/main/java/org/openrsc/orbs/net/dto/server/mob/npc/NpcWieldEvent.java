package org.openrsc.orbs.net.dto.server.mob.npc;

import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.openrsc.orbs.net.dto.server.mob.npc.model.NpcUpdateType;

@SuperBuilder
@Getter
public class NpcWieldEvent extends NpcUpdateEvent {
    private final NpcUpdateType type = NpcUpdateType.NPC_WIELD;
    private final int wieldAppearanceId1;
    private final int wieldAppearanceId2;
}
