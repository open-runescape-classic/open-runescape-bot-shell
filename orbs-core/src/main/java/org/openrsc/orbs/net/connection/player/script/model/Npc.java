package org.openrsc.orbs.net.connection.player.script.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Getter;
import org.openrsc.orbs.model.Locatable;
import org.openrsc.orbs.model.Point;
import org.openrsc.orbs.stateful.player.model.NpcInfo;

@Builder
@Getter
public class Npc implements Locatable {
    private final int npcId;
    private final int serverId;
    private final Point location;
    private final NpcInfo npcInfo;
    @JsonIgnore
    private final int walkingDistance;
}
