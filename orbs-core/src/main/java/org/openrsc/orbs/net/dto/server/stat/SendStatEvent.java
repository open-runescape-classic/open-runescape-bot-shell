package org.openrsc.orbs.net.dto.server.stat;

import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.openrsc.orbs.net.annotation.PacketSerializable;
import org.openrsc.orbs.net.annotation.RegisterEventOpcode;
import org.openrsc.orbs.net.annotation.TypeOverride;
import org.openrsc.orbs.net.annotation.model.DataType;
import org.openrsc.orbs.net.dto.server.Event;
import org.openrsc.orbs.net.opcode.ServerOpcode;

@SuperBuilder
@Getter
@PacketSerializable
@RegisterEventOpcode(ServerOpcode.SEND_STAT)
public class SendStatEvent extends Event {
    @TypeOverride(DataType.BYTE)
    private final int statId;
    @TypeOverride(DataType.BYTE)
    private final int currentLevel;
    @TypeOverride(DataType.BYTE)
    private final int maxLevel;
    private final int experience;
}
