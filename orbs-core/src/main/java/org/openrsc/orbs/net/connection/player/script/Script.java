package org.openrsc.orbs.net.connection.player.script;

import lombok.Getter;
import org.openrsc.orbs.net.connection.player.script.params.ScriptParam;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public abstract class Script<T> implements Runnable {
    @Getter
    public final ScriptAPI api;

    @Getter
    private T params;

    @Getter
    private boolean finished = false;

    protected Script(ScriptAPI api) {
        this.api = api;
    }

    /**
     * Use this method to do any post-processing of the parameters after receiving from front-end
     *
     * @param params script parameters
     */
    public void init(T params) {
        this.params = params;
    }

    /**
     * Called repetitively while as long as the script is running. It is recommended that you minimize the number
     * of actions per call to this method so that shutdown can be started expediently.
     */
    public abstract void run();

    /**
     * Called repetitively to shutdown gracefully until the script calls setFinished(true).
     * As general practice, it is best to use this to exit any underground/special area and return
     * to the main floor out in the open so that the next script can get wherever it needs to go.
     */
    public abstract void shutdown();

    /**
     * Override this method to provide meaningful metrics for your script (i.e. ores mined, bars smelted, etc)
     *
     * @return map of metric name(i.e. "bars smelted") to metric value (i.e. "1337")
     */
    public Map<String, String> getMetrics() {
        return Collections.emptyMap();
    }

    /**
     * Override this method to tell the frontend how to create the form for front-end users to configure the parameters
     *
     * @return script params to display on the front-end
     */
    public List<ScriptParam> getOptions() {
        return Collections.emptyList();
    }

    /**
     * Provides the default parameters for your script. These will be reflected on the front-end if there is a
     * matching ScriptParam returned by getOptions()
     *
     * @return default script parameters
     */
    public abstract T getDefaultParameters();

    public void done() {
        finished = true;
    }
}
