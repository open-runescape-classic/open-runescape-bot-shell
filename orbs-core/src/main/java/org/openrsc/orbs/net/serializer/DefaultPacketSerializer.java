package org.openrsc.orbs.net.serializer;

import io.netty.buffer.ByteBuf;
import lombok.extern.log4j.Log4j2;
import org.openrsc.orbs.net.annotation.RegisterActionOpcode;
import org.openrsc.orbs.net.annotation.TypeOverride;
import org.openrsc.orbs.net.annotation.model.DataType;
import org.openrsc.orbs.net.builder.PacketBuilder;
import org.openrsc.orbs.net.model.Packet;
import org.openrsc.orbs.net.opcode.ClientOpcode;
import org.openrsc.orbs.net.util.DataConversions;
import org.springframework.util.StringUtils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Log4j2
public class DefaultPacketSerializer<T> implements PacketSerializer<T>, PacketDeserializer<T> {
    private final DataTypeResolver dataTypeResolver = new DataTypeResolver();

    @Override
    @SuppressWarnings("unchecked")
    public T deserialize(Packet packet, Class<?> type) {
        Field[] fieldArray = type.getDeclaredFields();

        Map<String, Object> fieldToInstance = new HashMap<>();
        Map<String, Class<?>> fieldToOriginal = new HashMap<>();
        for (Field field : fieldArray) {
            String name = field.getName();
            fieldToOriginal.put(name, field.getType());
            DataTypeConverter conversion = getDataConversion(field);

            DataType resolvedType = conversion.getProtocolType();
            switch (resolvedType) {
                case BYTE_ARRAY -> fieldToInstance.put(name, packet.readBytes(packet.getReadableBytes()));
                case BYTE -> fieldToInstance.put(name, packet.readByte());
                case UNSIGNED_SHORT -> fieldToInstance.put(name, packet.readUnsignedShort());
                case SHORT -> fieldToInstance.put(name, packet.readShort());
                case INT -> fieldToInstance.put(name, packet.readInt());
                case LONG -> fieldToInstance.put(name, packet.readLong());
                case ENCRYPTED_STRING -> fieldToInstance.put(name, DataConversions.getEncryptedString(packet));
                case STRING -> fieldToInstance.put(name, packet.readString());
                case OBJECT -> {
                    PacketDeserializer<?> deserializer = SerializerUtils.getDeserializer(field.getType());
                    fieldToInstance.put(
                            name,
                            deserializer.deserialize(packet, field.getType())
                    );
                }
                case LIST -> {
                    ParameterizedType parameterizedType = (ParameterizedType) field.getGenericType();
                    Class<?> genericType = (Class<?>) parameterizedType.getActualTypeArguments()[0];
                    try {
                        //TODO: Implement this for things other than Strings. Sorry future me.
                        List<String> objectList = new ArrayList<>(5);
                        int sizeHeader = Byte.toUnsignedInt(packet.readByte());
                        for (int i = 0; i < sizeHeader; i++) {
                            objectList.add(packet.readString());
                        }
                        fieldToInstance.put(
                                name,
                                objectList
                        );
                    } catch (Exception e) {
                        log.catching(e);
                    }
                }
            }
        }

        try {
            Method builderMethod = type.getMethod("builder");
            Object builder = builderMethod.invoke(null);
            Class<?> builderType = builder.getClass();
            fieldToInstance.forEach((propName, value) -> {
                try {
                    Class<?> fieldType = fieldToOriginal.get(propName);
                    Method propMethod = builderType.getMethod(propName, fieldType);
                    propMethod.setAccessible(true);
                    if (value instanceof Byte && fieldType.getName().equals("boolean")) {
                        value = ((Byte) value).intValue() == 1;
                    }
                    if (value instanceof Byte && fieldType.getName().equals("int")) {
                        value = Byte.toUnsignedInt((Byte) value);
                    }
                    propMethod.invoke(builder, value);
                } catch (Exception e) {
                    throw new RuntimeException(
                            MessageFormat.format("Unable to set property name {0}", propName),
                            e
                    );
                }
            });
            Method methodBuild = builderType.getMethod("build");
            methodBuild.setAccessible(true);

            return (T) methodBuild.invoke(builder);
        } catch (Exception e) {
            throw new RuntimeException(
                    MessageFormat.format("Error creating type {0}", type.getSimpleName()),
                    e
            );
        }
    }

    @Override
    public Packet serialize(T object) {
        PacketBuilder packetBuilder = new PacketBuilder();

        Class<?> type = object.getClass();
        RegisterActionOpcode opcode = type.getAnnotation(RegisterActionOpcode.class);
        Optional.ofNullable(opcode).ifPresent(code -> {
            ClientOpcode clientOpcode = code.value();
            packetBuilder.setID(clientOpcode.getOpCode());
        });

        Field[] fields = type.getDeclaredFields();
        for (Field field : fields) {
            Class<?> fieldType = field.getType();
            String fieldName = field.getName();
            String methodName = "get" + StringUtils.capitalize(fieldName);
            try {
                Method fieldAccessor = type.getMethod(methodName);
                Object invocationResult = fieldAccessor.invoke(object);

                // Get converter
                DataTypeConverter converter = getDataConversion(field);
                Object convertedInstance = converter.toProtocolType(invocationResult);
                if (converter.getProtocolType() == DataType.OBJECT || converter.getProtocolType() == DataType.LIST) {
                    if (fieldType.isAssignableFrom(String.class)) {
                        packetBuilder.writeString((String) invocationResult);
                    } else if (List.class.isAssignableFrom(fieldType)) {
                        List<?> list = (List<?>) invocationResult;
                        for (Object obj : list) {
                            PacketSerializer serializer = SerializerUtils.getSerializer(obj.getClass());
                            Packet packet = serializer.serialize(obj);
                            ByteBuf buffer = packet.getBuffer();
                            packetBuilder.writeBytes(buffer.array());
                        }
                    } else {
                        PacketSerializer serializer = SerializerUtils.getSerializer(object.getClass());
                        Packet packet = serializer.serialize(invocationResult);
                        ByteBuf buffer = packet.getBuffer();
                        packetBuilder.writeBytes(buffer.array());
                    }
                } else {
                    switch (converter.getProtocolType()) {
                        case BYTE -> packetBuilder.writeByte((Byte) convertedInstance);
                        case SHORT, UNSIGNED_SHORT -> packetBuilder.writeShort((Short) convertedInstance);
                        case INT -> packetBuilder.writeInt((Integer) convertedInstance);
                        case LONG -> packetBuilder.writeLong((Long) convertedInstance);
                    }
                }
            } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return packetBuilder.toPacket();
    }


    private DataTypeConverter getDataConversion(Field field) {
        DataTypeConverter conversion;
        if (field.isAnnotationPresent(TypeOverride.class)) {
            TypeOverride override = field.getAnnotation(TypeOverride.class);
            conversion = dataTypeResolver.getType(field.getType(), override);
        } else {
            conversion = dataTypeResolver.getType(field.getType());
        }
        return conversion;
    }
}
