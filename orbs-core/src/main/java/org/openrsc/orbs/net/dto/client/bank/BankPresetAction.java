package org.openrsc.orbs.net.dto.client.bank;

public interface BankPresetAction {
    int getPresetSlot();
}
