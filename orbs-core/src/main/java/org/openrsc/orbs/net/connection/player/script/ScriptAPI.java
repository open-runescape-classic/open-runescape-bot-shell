package org.openrsc.orbs.net.connection.player.script;

import com.google.common.eventbus.EventBus;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.ArrayUtils;
import org.mapstruct.factory.Mappers;
import org.openrsc.orbs.model.InventoryItem;
import org.openrsc.orbs.model.Locatable;
import org.openrsc.orbs.model.Point;
import org.openrsc.orbs.model.Skill;
import org.openrsc.orbs.model.TileTraversalProvider;
import org.openrsc.orbs.net.connection.player.PlayerConnection;
import org.openrsc.orbs.net.connection.player.script.event.ScriptEvent;
import org.openrsc.orbs.net.connection.player.script.model.Item;
import org.openrsc.orbs.net.connection.player.script.model.ItemSet;
import org.openrsc.orbs.net.connection.player.script.model.Npc;
import org.openrsc.orbs.net.connection.player.script.model.Player;
import org.openrsc.orbs.net.connection.player.script.model.SlottedItem;
import org.openrsc.orbs.net.dto.Step;
import org.openrsc.orbs.net.dto.client.bank.CloseBankAction;
import org.openrsc.orbs.net.dto.client.bank.DepositAllFromInventoryAction;
import org.openrsc.orbs.net.dto.client.bank.DepositItemAction;
import org.openrsc.orbs.net.dto.client.bank.WithdrawItemAction;
import org.openrsc.orbs.net.dto.client.item.GroundItemTakeAction;
import org.openrsc.orbs.net.dto.client.item.ItemOnItemAction;
import org.openrsc.orbs.net.dto.client.item.ItemOnSceneryAction;
import org.openrsc.orbs.net.dto.client.item.UseItemAction;
import org.openrsc.orbs.net.dto.client.npc.AttackNpcAction;
import org.openrsc.orbs.net.dto.client.npc.ChooseMenuOptionAction;
import org.openrsc.orbs.net.dto.client.npc.TalkToNpcAction;
import org.openrsc.orbs.net.dto.client.object.UseObjectAction;
import org.openrsc.orbs.net.dto.client.trade.AcceptTradeAction;
import org.openrsc.orbs.net.dto.client.trade.OfferItemsAction;
import org.openrsc.orbs.net.dto.client.trade.RejectTradeAction;
import org.openrsc.orbs.net.dto.client.trade.SendTradeRequestAction;
import org.openrsc.orbs.net.dto.client.trade.TradeItem;
import org.openrsc.orbs.net.dto.client.walk.WalkToPointAction;
import org.openrsc.orbs.net.mapper.net.TradeItemMapper;
import org.openrsc.orbs.net.mapper.script.PlayerMapper;
import org.openrsc.orbs.net.model.Packet;
import org.openrsc.orbs.net.serializer.SerializerUtils;
import org.openrsc.orbs.service.PlayerService;
import org.openrsc.orbs.stateful.objects.GameObject;
import org.openrsc.orbs.stateful.player.PlayerState;
import org.openrsc.orbs.stateful.player.model.NpcInfo;
import org.openrsc.orbs.stateful.player.model.NpcLocation;
import org.openrsc.orbs.stateful.trade.TradeStatus;
import org.openrsc.orbs.util.World;
import org.openrsc.orbs.util.pathfinder.Path;
import org.openrsc.orbs.util.pathfinder.PathFinder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeSet;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@Log4j2
public class ScriptAPI implements AutoCloseable {
    public static final int LONG_DISTANCE_PATHFINDER_MAX_ITER = 100000;
    private final EventBus scriptEventBus;
    private final PlayerState playerState;
    private final PlayerConnection playerConnection;
    private final TileTraversalProvider tileProvider;
    private final World world;
    private final TradeItemMapper itemMapper = Mappers.getMapper(TradeItemMapper.class);
    private final PlayerMapper playerMapper = Mappers.getMapper(PlayerMapper.class);

    public ScriptAPI(
            EventBus scriptEventBus,
            PlayerService playerService,
            PlayerState playerState,
            PlayerConnection playerConnection,
            World world
    ) {
        scriptEventBus.register(this);
        this.scriptEventBus = scriptEventBus;
        this.playerState = playerState;
        this.playerConnection = playerConnection;
        this.world = world;
        this.tileProvider = playerService.getTileProvider(playerState);
    }

    public void setStatus(String status) {
        playerState.setStatus(status);
    }

    public Point getLocation() {
        return playerState.getLocation();
    }

    public void walkToNearestBank() {
        for (Locations nearestBank : Locations.getNearestBanks(getLocation())) {
            Point bankLocation = nearestBank.getLocation();
            if (isReachable(bankLocation)) {
                walkTo(bankLocation);
                break;
            }
        }
    }

    public void walkTo(Locatable locatable) {
        walkTo(locatable.getLocation());
    }

    public void walkTo(int x, int y) {
        walkTo(new Point(x, y));
    }

    public void walkTo(Point destination) {
        withErrorHandling(() -> {
            PathFinder worldPathFinder = new PathFinder(
                    world::getTileTraversal,
                    playerState.getLocation(),
                    destination,
                    LONG_DISTANCE_PATHFINDER_MAX_ITER
            );
            Path worldPath = worldPathFinder.findPath();
            if (worldPath != null) {
                // Split into smaller goals
                List<Point> interpolated = new ArrayList<>();
                int i = 0;
                for (Point waypoint : worldPath.getPoints()) {
                    if (i % 10 == 0) {
                        interpolated.add(waypoint);
                    }
                    i++;
                }
                interpolated.add(worldPath.getPoints().getLast());

                for (Point waypoint : interpolated) {
                    if (!localWalkTo(waypoint, true)) {
                        return;
                    }
                }
            } else {
                playerState.getMessageHistory().add("Could not find path to " + destination);
            }
        });
    }

    public boolean isReachable(Locatable locatable) {
        return isReachable(locatable.getLocation());
    }

    public boolean isReachable(int x, int y) {
        return isReachable(new Point(x, y));
    }

    public boolean isReachable(Point destination) {
        PathFinder finder = new PathFinder(
                tileProvider,
                getLocation(),
                destination,
                100000
        );
        return finder.findPath() != null;
    }

    public boolean isVisible(Locatable locatable) {
        return isVisible(locatable.getLocation());
    }

    public boolean isVisible(int x, int y) {
        return isVisible(new Point(x, y));
    }

    public boolean isVisible(Point destination) {
        PathFinder finder = new PathFinder(
                tileProvider,
                getLocation(),
                destination
        );
        return finder.findPath() != null;
    }

    private boolean localWalkTo(Point destination, boolean blocking) {
        log.debug("Walking to " + destination + " from " + getLocation());
        PathFinder pathFinder = new PathFinder(
                tileProvider,
                playerState.getLocation(),
                destination
        );
        Path newPath = pathFinder.findPath();
        WalkToPointAction.WalkToPointActionBuilder builder = WalkToPointAction.builder();
        if (newPath != null) {
            Point endPoint = newPath.getPoints().getLast();
            Point startPoint = newPath.getPoints().poll();
            if (startPoint != null) {
                builder.start(startPoint.toDTO());
                Point point;
                while ((point = newPath.getPoints().poll()) != null) {
                    int offsetX = point.getX() - startPoint.getX();
                    int offsetY = point.getY() - startPoint.getY();

                    builder.step(new Step(offsetX, offsetY));
                }
            }
            WalkToPointAction action = builder.build();
            Packet packet = SerializerUtils.getSerializer(WalkToPointAction.class).serialize(action);
            playerConnection.writePacket(packet);
            if (blocking) {
                pauseUntil(() -> playerState.getLocation().equals(endPoint), action.getSteps().size() * 1000);
            }
            return true;
        }
        log.info("No path found to destination " + destination);
        return false;
    }

    public void pauseUntil(Supplier<Boolean> condition, int maxWait) {
        int wait = 0;
        int SLEEP_INTERVAL = 50;
        while (!condition.get() && wait < maxWait) {
            pause(SLEEP_INTERVAL);
            wait += SLEEP_INTERVAL;
        }
    }

    public void repeatUntil(Supplier<Boolean> condition, Runnable action, int maxWait, int retry) {
        int INTERVAL = 20;

        int numSteps = maxWait / INTERVAL;
        int remainder = maxWait % INTERVAL;
        int numStepsTilAction = retry / INTERVAL;
        action.run();
        for (int i = 0; i <= numSteps && !condition.get(); i++) {
            if (i % numStepsTilAction == 0) {
                action.run();
            }
            if (i == numSteps) {
                pause(remainder);
                return;
            }
            pause(INTERVAL);
        }
    }

    public void useObject(GameObject gameObject) {
        useObjectAt(gameObject.getLocation());
    }

    public void useObjectAt(int x, int y) {
        useObjectAt(new Point(x, y));
    }

    public void useObjectAt(Point point) {
        afterWalkTo(point, () -> {
            UseObjectAction action = UseObjectAction.builder().location(point.toDTO()).build();
            Packet packet = SerializerUtils.getSerializer(UseObjectAction.class).serialize(action);
            playerConnection.writePacket(packet);
//            AtomicInteger i = new AtomicInteger();
//            pauseUntil(() -> {
//                i.getAndIncrement();
//                Point location = getLocation();
//                if (i.get() % 30 == 0) {
//                    log.info("Waiting..." + location.distance(point));
//                }
//                return location.distance(point) <= 2;
//            }, 6000);
        });
    }

    public void useInventoryItem(int slotId) {
        UseItemAction action = UseItemAction.builder().slotId(slotId).commandIndex(0).amount(1).build();
        Packet packet = SerializerUtils.getSerializer(UseItemAction.class).serialize(action);
        playerConnection.writePacket(packet);
    }

    public void useItemOnItem(int slotId1, int slotId2) {
        ItemOnItemAction action = ItemOnItemAction.builder().slotIndex1(slotId1).slotIndex2(slotId2).build();
        Packet packet = SerializerUtils.getSerializer(ItemOnItemAction.class).serialize(action);
        playerConnection.writePacket(packet);
    }

    public void sleepIfFatigueAbove(int fatigue) {
        Integer playerFatigue = playerState.getFatigue().get();
        if (playerFatigue != null && playerFatigue >= fatigue) {
            sleep();
        }
    }

    public void sleep() {
        setStatus("Sleeping");
        repeatUntil(
                this::isSleeping,
                () -> findItemInInventory(CommonIds.ITEM_SLEEPING_BAG).ifPresent(this::useInventoryItem),
                5000,
                1000
        );
    }

    public void shutdown() {
        setStatus("Script Complete");
        playerConnection.shutdownScript();
    }

    public int walkingDistance(Point destination) {
        PathFinder worldPathFinder = new PathFinder(
                tileProvider,
                playerState.getLocation(),
                destination,
                50000
        );
        Path worldPath = worldPathFinder.findPath();
        if (worldPath != null) {
            return worldPath.getPoints().size();
        }
        return Integer.MAX_VALUE;
    }

    public int walkingDistance(Locatable locatable) {
        return walkingDistance(locatable.getLocation());
    }

    private boolean isSleeping() {
        return playerState.getIsSleeping().get();
    }

    public void pause(long ms) {
        try {
            Thread.sleep(ms);
        } catch (Exception ignored) {
        }
    }

    public TreeSet<Npc> findNpcs(int... npcIds) {
        TreeSet<Npc> npcs = new TreeSet<>(Comparator.comparingInt(Npc::getWalkingDistance));
        Map<Integer, NpcLocation> npcsByServerId = new HashMap<>(playerState.getNpcsByServerId());

        npcsByServerId.entrySet().stream()
                .filter(entry -> ArrayUtils.contains(npcIds, entry.getValue().getNpcId()))
                .forEach(entry -> {
                    Integer serverIndex = entry.getKey();
                    NpcLocation npcLocation = entry.getValue();
                    NpcInfo npcInfo = playerState.getNpcInfoByServerId().get(serverIndex);

                    Npc npc = Npc.builder()
                            .serverId(serverIndex)
                            .npcId(npcLocation.getNpcId())
                            .location(npcLocation.getLocation())
                            .walkingDistance(walkingDistance(npcLocation))
                            .npcInfo(npcInfo)
                            .build();

                    if (npc.getWalkingDistance() != Integer.MAX_VALUE) {
                        npcs.add(npc);
                    }
                });

        return npcs;
    }

    public Optional<Npc> findNearestNpc(int... npcIds) {
        TreeSet<Npc> npcs = findNpcs(npcIds);
        if (npcs.isEmpty()) {
            return Optional.empty();
        } else {
            return Optional.of(npcs.first());
        }
    }

    public Optional<GameObject> findNearestWallObject(int... objIds) {
        return withErrorHandling(() -> {
            TreeSet<GameObject> candidates = new TreeSet<>(Comparator.comparingInt(GameObject::getWalkingDistance));

            Map<Point, GameObject> boundaries = playerState.getGameObjects().getBoundaries();
            candidates.addAll(
                    boundaries.values()
                            .stream()
                            .filter(obj -> ArrayUtils.contains(objIds, obj.getId()))
                            .map(obj -> obj.toBuilder().walkingDistance(walkingDistance(obj)).build())
                            .collect(Collectors.toSet())
            );

            try {
                return Optional.of(candidates.first());
            } catch (Exception e) {
                return Optional.empty();
            }
        });
    }

    public Optional<GameObject> findNearestObject(Collection<Integer> objIds) {
        return findNearestObject(objIds.stream().mapToInt(Integer::intValue).toArray());
    }

    public Optional<GameObject> findNearestObject(int... objIds) {
        final long start = System.currentTimeMillis();
        return withErrorHandling(() -> {
            TreeSet<GameObject> candidates = new TreeSet<>(Comparator.comparingInt(obj -> getLocation().distance(obj.getLocation())));

            candidates.addAll(
                    playerState.getGameObjects().getSceneries().values()
                            .stream()
                            .filter(obj -> ArrayUtils.contains(objIds, obj.getId()))
                            .collect(Collectors.toSet())
            );

            for (GameObject candidate : candidates) {
                if (isReachable(candidate)) {
                    return Optional.of(candidate);
                }
            }
            return Optional.empty();
        });
    }

    public Optional<Integer> findItemInInventory(int itemId) {
        List<InventoryItem> bySlot = playerState.getInventory().getBySlot();
        for (int i = 0; i < bySlot.size(); i++) {
            InventoryItem inventoryItem = bySlot.get(i);
            if (inventoryItem.getDef().getId() == itemId) {
                return Optional.of(i);
            }
        }
        return Optional.empty();
    }

    public void attackNpc(Npc npc) {
        afterWalkTo(npc, () -> {
            AttackNpcAction action = AttackNpcAction.builder()
                    .serverIndex(npc.getServerId())
                    .build();
            Packet packet = SerializerUtils.getSerializer(AttackNpcAction.class).serialize(action);
            playerConnection.writePacket(packet);
        });
    }

    public void talkToNpc(Npc npc) {
        afterWalkTo(npc, () -> {
            TalkToNpcAction action = TalkToNpcAction.builder()
                    .serverIndex(npc.getServerId())
                    .build();
            Packet packet = SerializerUtils.getSerializer(TalkToNpcAction.class).serialize(action);
            playerConnection.writePacket(packet);
        });
    }

    public boolean inMenu() {
        return playerState.getIsInMenu().get();
    }

    public List<String> getMenuOptions() {
        return Collections.unmodifiableList(playerState.getCurrentMenuOptions());
    }

    public void chooseOption(int option) {
        ChooseMenuOptionAction action = ChooseMenuOptionAction.builder()
                .option(option)
                .build();
        Packet packet = SerializerUtils.getSerializer(ChooseMenuOptionAction.class).serialize(action);
        playerConnection.writePacket(packet);
        playerState.getIsInMenu().set(false);
    }

    public boolean isInCombat() {
        return playerState.getIsInCombat().get();
    }

    public boolean openBank() {
        int MAX_TRIES = 5;
        int attempts = 0;
        while (!isInBank() && attempts < MAX_TRIES) {
            attempts++;
            findNearestNpc(CommonIds.NPC_BANKER_IDS)
                    .ifPresent(npc -> {
                        talkToNpc(npc);
                        pauseUntil(this::inMenu, 5000);
                        if (inMenu()) {
                            chooseOption(0);
                            pauseUntil(this::isInBank, 5000);
                        }
                    });
        }
        return isInBank();
    }

    public boolean isInBank() {
        return playerState.getBank().isOpen();
    }

    public void closeBank() {
        CloseBankAction action = CloseBankAction.builder().build();
        Packet packet = SerializerUtils.getSerializer(CloseBankAction.class).serialize(action);
        playerConnection.writePacket(packet);
    }

    public int getBankCount(int itemId) {
        Integer count = playerState.getBank().getItemIdToAmount().get(itemId);
        return count == null ? 0 : count;
    }

    public List<SlottedItem> getInventory() {
        List<InventoryItem> bySlot = new ArrayList<>(playerState.getInventory().getBySlot());
        List<SlottedItem> slottedItems = new ArrayList<>(bySlot.size());
        for (int i = 0; i < bySlot.size(); i++) {
            InventoryItem current = bySlot.get(i);
            slottedItems.add(
                    SlottedItem.builder()
                            .id(current.getDef().getId())
                            .slotId(i)
                            .amount(current.getAmount())
                            .noted(current.isNoted())
                            .build()
            );
        }
        return slottedItems;
    }

    public int getInventoryCount(Integer... itemIds) {
        return playerState.getInventory().getBySlot().stream()
                .filter(invItem -> ArrayUtils.contains(itemIds, invItem.getDef().getId()))
                .mapToInt(InventoryItem::getAmount)
                .sum();
    }

    public void withdraw(int itemId, int amount) {
        WithdrawItemAction action = WithdrawItemAction.builder()
                .catalogId(itemId)
                .amount(amount)
                .build();
        Packet packet = SerializerUtils.getSerializer(WithdrawItemAction.class).serialize(action);
        playerConnection.writePacket(packet);
        pauseUntil(() -> getInventoryCount(itemId) == amount, 2000);
    }

    public void withdraw(ItemSet itemSet) {
        for (Item item : itemSet.getItems()) {
            withdraw(item.getId(), item.getAmount());
        }
    }

    public void depositAll() {
        DepositAllFromInventoryAction action = DepositAllFromInventoryAction.builder().build();
        Packet packet = SerializerUtils.getSerializer(DepositAllFromInventoryAction.class).serialize(action);
        playerConnection.writePacket(packet);
        pauseUntil(() -> getInventory().size() == 0, 2000);
    }

    public void depositAllExcept(int... itemIds) {
        Map<Integer, Integer> invCount = new HashMap<>();
        for (SlottedItem item : getInventory()) {
            Integer existingCount = invCount.get(item.getId());
            if (existingCount == null) {
                invCount.put(item.getId(), item.getAmount());
            } else {
                invCount.put(item.getId(), existingCount + 1);
            }
        }

        invCount.entrySet()
                .stream()
                .filter(entry -> !ArrayUtils.contains(itemIds, entry.getKey()))
                .forEach(entry -> deposit(entry.getKey(), entry.getValue()));
    }

    public void depositAll(Integer... itemIds) {
        for (int itemId : itemIds) {
            deposit(itemId, getInventoryCount(itemId));
            pause(100);
        }
        pauseUntil(() -> getInventoryCount(itemIds) == 0, 2000);
    }

    public void deposit(int itemId, int amount) {
        DepositItemAction action = DepositItemAction.builder()
                .catalogId(itemId)
                .amount(amount)
                .build();
        Packet packet = SerializerUtils.getSerializer(DepositItemAction.class).serialize(action);
        playerConnection.writePacket(packet);
        pause(100);
    }

    private void withErrorHandling(Runnable runnable) {
        try {
            runnable.run();
        } catch (Exception e) {
            log.catching(e);
            e.printStackTrace();
        }
    }

    private <T> Optional<T> withErrorHandling(Supplier<Optional<T>> supplier) {
        try {
            return supplier.get();
        } catch (Exception e) {
            log.catching(e);
            e.printStackTrace();
        }
        return Optional.empty();
    }

    private void afterWalkTo(Point point, Runnable action) {
        if (localWalkTo(point, false)) {
            action.run();
        }
    }

    public Integer getMaxLevel(Skill skill) {
        return playerState.getSkills().getMaxLevels().get(skill);
    }

    public Integer getCurrentLevel(Skill skill) {
        return playerState.getSkills().getCurrentLevels().get(skill);
    }

    public Integer getFatigue() {
        return playerState.getFatigue().get();
    }

    private void afterWalkTo(Locatable locatable, Runnable action) {
        afterWalkTo(locatable.getLocation(), action);
    }

    @Override
    public void close() throws Exception {
        scriptEventBus.unregister(this);
    }

    public void displayMessage(String message) {
        playerState.getMessageHistory().add(message);
    }

    public void useItemOnObject(Integer slot, GameObject furnace) {
        ItemOnSceneryAction action = ItemOnSceneryAction.builder()
                .slotId(slot)
                .location(furnace.getLocation().toDTO())
                .build();
        Packet packet = SerializerUtils.getSerializer(ItemOnSceneryAction.class).serialize(action);
        playerConnection.writePacket(packet);
    }

    public void takeItem(Point location, int id) {
        GroundItemTakeAction action = GroundItemTakeAction.builder().location(location.toDTO()).itemId(id).build();
        Packet packet = SerializerUtils.getSerializer(GroundItemTakeAction.class).serialize(action);
        playerConnection.writePacket(packet);
    }

    public Optional<Player> getPlayerByUsername(String username) {
        final org.openrsc.orbs.stateful.player.model.Player player = playerState.getPlayerInfoByPid().values().stream()
                .filter(item -> item.getUsername().equalsIgnoreCase(username))
                .findAny()
                .orElse(null);
        if (player != null) {
            int playerId = player.getPlayerId();
            final Map<Integer, Point> playersByPid = playerState.getPlayersByPid();
            if (playersByPid.containsKey(playerId)) {
                Point location = playersByPid.get(playerId);
                return Optional.of(playerMapper.toScriptDTO(player, location));
            }
        }
        return Optional.empty();
    }

    /*
     * TRADING
     */

    /**
     * Sends a trade request to a given player
     *
     * @param playerId the target player id
     */
    public void sendTradeRequest(int playerId) {
        SendTradeRequestAction action = SendTradeRequestAction.builder().targetPlayerId(playerId).build();
        Packet packet = SerializerUtils.getSerializer(SendTradeRequestAction.class).serialize(action);
        playerConnection.writePacket(packet);
    }

    public void acceptTrade() {
        if (!playerState.getTrade().isAccepted()) {
            AcceptTradeAction action = AcceptTradeAction.builder().build();
            Packet packet = SerializerUtils.getSerializer(AcceptTradeAction.class).serialize(action);
            playerConnection.writePacket(packet);
        }
    }

    public void declineTrade() {
        RejectTradeAction action = RejectTradeAction.builder().build();
        Packet packet = SerializerUtils.getSerializer(RejectTradeAction.class).serialize(action);
        playerConnection.writePacket(packet);
    }

    public void setTradeOffer(ItemSet itemSet) {
        final OfferItemsAction.OfferItemsActionBuilder<?, ?> builder = OfferItemsAction.builder();
        List<TradeItem> tradeItems = itemMapper.toTradeItems(itemSet);
        for (TradeItem item : tradeItems) {
            builder.item(item);
        }
        OfferItemsAction action = builder.build();
        Packet packet = SerializerUtils.getSerializer(OfferItemsAction.class).serialize(action);
        playerConnection.writePacket(packet);
    }

    public boolean isTrading() {
        return isInTradeOfferScreen() || isInTradeConfirmScreen();
    }

    public boolean isInTradeOfferScreen() {
        return playerState.getTrade().getStatus() == TradeStatus.OFFER_WINDOW;
    }

    public boolean isInTradeConfirmScreen() {
        return playerState.getTrade().getStatus() == TradeStatus.CONFIRM_WINDOW;
    }

    public boolean isTradeAccepted() {
        return playerState.getTrade().isAccepted();
    }

    public boolean isOtherPlayerTradeAccepted() {
        return playerState.getTrade().isOtherPlayerAccepted();
    }

    /**
     * Broadcasts this event to all players on this instance. Use @Subscribe on a method with this object type
     * to receive it. See Guava EventBus for more details
     *
     * @param event a custom event object, e.g. OutOfSuppliesEvent or LocationUpdateEvent
     */
    public void broadcast(ScriptEvent event) {
        scriptEventBus.post(event);
    }

    public String getUsername() {
        return playerState.getUsername();
    }

    public int itemSetInInventory(ItemSet itemSet) {
        if (itemSet == null) {
            return 0;
        }
        return itemSet.getItems()
                .stream()
                .mapToInt(item -> {
                    int amountInInv = getInventoryCount(item.getId());
                    return amountInInv / item.getAmount();
                })
                .min()
                .orElse(0);
    }
}
