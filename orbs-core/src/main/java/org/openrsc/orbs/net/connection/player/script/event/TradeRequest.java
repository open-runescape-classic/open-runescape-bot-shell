package org.openrsc.orbs.net.connection.player.script.event;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.openrsc.orbs.net.connection.player.script.model.Player;

@AllArgsConstructor
@Getter
public class TradeRequest {
    private final Player player;
}
