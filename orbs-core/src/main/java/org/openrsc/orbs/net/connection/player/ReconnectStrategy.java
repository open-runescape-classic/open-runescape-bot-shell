package org.openrsc.orbs.net.connection.player;

public enum ReconnectStrategy {
    IMMEDIATE,
    WAIT_ONE_MINUTE,
    WAIT_FIVE_MINUTES,
    NO_RETRY
}
