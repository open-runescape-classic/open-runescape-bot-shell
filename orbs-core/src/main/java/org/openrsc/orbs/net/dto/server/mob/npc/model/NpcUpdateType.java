package org.openrsc.orbs.net.dto.server.mob.npc.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor
@Getter
public enum NpcUpdateType {
    CHAT_MESSAGE(1),
    //	updates.add((short) (chatMessage.getRecipient() == null ? -1 : chatMessage.getRecipient().getIndex()));
    //	updates.add(chatMessage.getMessageString());
    DAMAGE_UPDATE(2),
    //  updates.add((byte) npcNeedingHitsUpdate.getDamage());
    //	updates.add((byte) npcNeedingHitsUpdate.getCurHits());
    //	updates.add(((byte) npcNeedingHitsUpdate.getMaxHits()));
    PROJECTILE_NPC(3),
    //	updates.add((short) projectile.getType());
    //  updates.add((short) victim.getIndex());
    PROJECTILE_PLAYER(4),
    //	updates.add((short) projectile.getType());
    //	updates.add((short) victim.getIndex());
    NPC_SKULLS(5),
    //  updates.add((byte) npcNeedingSkullUpdate.getSkull());
    NPC_WIELD(6),
    //  updates.add((byte) npcNeedingWieldUpdate.getWield());
    //	updates.add((byte) npcNeedingWieldUpdate.getWield2());
    BUBBLE(7)
    //  updates.add((short) npcNeedingBubbleUpdate.getID());
    ;

    private static final Map<Integer, NpcUpdateType> byId = new HashMap<>();

    static {
        Arrays.stream(NpcUpdateType.values()).forEach(type -> byId.put(type.getUpdateType(), type));
    }

    private final int updateType;

    public static NpcUpdateType byId(int id) {
        return byId.get(id);
    }
}
