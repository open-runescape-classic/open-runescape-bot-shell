package org.openrsc.orbs.net.dto.client.social;

public interface SocialAction {
    String getPlayer();
}
