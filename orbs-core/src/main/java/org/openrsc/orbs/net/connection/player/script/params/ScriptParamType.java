package org.openrsc.orbs.net.connection.player.script.params;

public enum ScriptParamType {
    STRING,
    NUMBER,
    ENUM,
    INSTRUCTIONS,
    BOOLEAN
}
