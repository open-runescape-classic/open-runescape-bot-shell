package org.openrsc.orbs.net.dto.server.mob.player.appearances;

import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.openrsc.orbs.net.dto.server.mob.player.appearances.model.PlayerUpdateType;
import org.openrsc.orbs.net.dto.server.mob.player.appearances.model.ProjectileType;

@SuperBuilder
@Getter
public class PlayerOnNpcProjectileEvent extends PlayerUpdateEvent {
    private final PlayerUpdateType type = PlayerUpdateType.PROJECTILE_PLAYER;
    private final ProjectileType projectileType;
    private final int victimServerIndex;
}