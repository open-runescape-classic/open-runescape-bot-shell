package org.openrsc.orbs.net.dto.client.trade;

import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.openrsc.orbs.net.annotation.PacketSerializable;
import org.openrsc.orbs.net.annotation.RegisterActionOpcode;
import org.openrsc.orbs.net.dto.client.Action;
import org.openrsc.orbs.net.opcode.ClientOpcode;

@SuperBuilder
@Getter
@RegisterActionOpcode(ClientOpcode.PLAYER_ACCEPTED_INIT_TRADE_REQUEST)
@PacketSerializable
public class AcceptTradeAction extends Action {
}
