package org.openrsc.orbs.net.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import org.openrsc.orbs.net.annotation.PacketSerializable;
import org.openrsc.orbs.net.annotation.TypeOverride;
import org.openrsc.orbs.net.annotation.model.DataType;

@AllArgsConstructor
@Getter
@PacketSerializable
@ToString
public class Point {
    @TypeOverride(DataType.SHORT)
    private final int x;
    @TypeOverride(DataType.SHORT)
    private final int y;
}
