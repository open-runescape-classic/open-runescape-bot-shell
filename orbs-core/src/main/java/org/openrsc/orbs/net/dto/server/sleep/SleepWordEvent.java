package org.openrsc.orbs.net.dto.server.sleep;

import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.openrsc.orbs.net.annotation.PacketSerializable;
import org.openrsc.orbs.net.annotation.RegisterEventOpcode;
import org.openrsc.orbs.net.dto.server.Event;
import org.openrsc.orbs.net.opcode.ServerOpcode;

@SuperBuilder
@Getter
@PacketSerializable
@RegisterEventOpcode(ServerOpcode.SEND_SLEEPSCREEN)
public class SleepWordEvent extends Event {
    private final byte[] image;
}
