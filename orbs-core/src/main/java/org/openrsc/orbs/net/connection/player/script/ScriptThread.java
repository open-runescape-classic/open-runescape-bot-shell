package org.openrsc.orbs.net.connection.player.script;

import com.google.common.eventbus.EventBus;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.openrsc.orbs.util.ThreadUtils;

@Log4j2
public class ScriptThread extends Thread {
    @Getter
    private final Script<?> script;
    private final ScriptAPI api;
    private final EventBus scriptEventBus;
    private final Object pauseLock = new Object();
    @Getter
    private ScriptState scriptState = ScriptState.RUNNING;
    @Getter
    private long timeElapsedMs = 0;
    private final Thread elapsedTimeCounter = new Thread(() -> {
        while (scriptState != ScriptState.KILLED && scriptState != ScriptState.FINISHED) {
            ThreadUtils.sleep(100);
            if (scriptState == ScriptState.RUNNING) {
                timeElapsedMs += 100;
            }
        }
    });

    public ScriptThread(Script<?> script, ScriptAPI api, EventBus scriptEventBus) {
        this.script = script;
        this.api = api;
        this.scriptEventBus = scriptEventBus;
    }

    @Override
    public void run() {
        elapsedTimeCounter.start();
        scriptEventBus.register(script);
        while (!script.isFinished()) {
            try {
                checkForPaused();
                api.pause(200);
                if (scriptState == ScriptState.SLEEPING) {
                    api.setStatus("Sleeping");
                } else if (scriptState == ScriptState.KILLED) {
                    break;
                } else if (scriptState == ScriptState.SHUTTING_DOWN) {
                    script.shutdown();
                } else {
                    script.run();
                }
            } catch (Exception ex) {
                log.catching(ex);
                ex.printStackTrace();
            }
        }
        scriptState = ScriptState.FINISHED;
        scriptEventBus.unregister(script);
    }

    private void checkForPaused() {
        synchronized (pauseLock) {
            while (scriptState == ScriptState.PAUSED || scriptState == ScriptState.SLEEPING) {
                try {
                    scriptEventBus.unregister(script);
                    pauseLock.wait();
                    scriptEventBus.register(script);
                } catch (Exception ex) {
                    log.catching(ex);
                    ex.printStackTrace();
                }
            }
        }
    }

    public void pauseThread() {
        scriptState = ScriptState.PAUSED;
    }

    public boolean isPaused() {
        return scriptState == ScriptState.PAUSED;
    }

    public void resumeThread() {
        synchronized (pauseLock) {
            scriptState = ScriptState.RUNNING;
            pauseLock.notify();
        }
    }

    public void shutdownImmediately() {
        scriptState = ScriptState.KILLED;
    }

    public void shutdown() {
        scriptState = ScriptState.SHUTTING_DOWN;
    }

    public void sleep() {
        scriptState = ScriptState.SLEEPING;
    }
}
