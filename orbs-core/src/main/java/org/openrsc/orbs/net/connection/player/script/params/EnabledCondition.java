package org.openrsc.orbs.net.connection.player.script.params;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class EnabledCondition {
    private final String key;
    private final Object value;
}
