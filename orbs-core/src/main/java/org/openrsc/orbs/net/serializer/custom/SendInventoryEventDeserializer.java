package org.openrsc.orbs.net.serializer.custom;

import org.openrsc.orbs.model.InventoryItem;
import org.openrsc.orbs.model.ItemDef;
import org.openrsc.orbs.net.dto.server.inventory.SendInventoryEvent;
import org.openrsc.orbs.net.model.Packet;
import org.openrsc.orbs.net.serializer.PacketDeserializer;
import org.openrsc.orbs.util.EntityService;

import java.util.ArrayList;
import java.util.List;

public class SendInventoryEventDeserializer implements PacketDeserializer<SendInventoryEvent> {
    @Override
    public SendInventoryEvent deserialize(Packet packet, Class<?> type) {
        int inventorySize = Byte.toUnsignedInt(packet.readByte());
        List<InventoryItem> inventoryItems = new ArrayList<>(inventorySize);
        for (int i = 0; i < inventorySize; i++) {
            InventoryItem.InventoryItemBuilder itemBuilder = InventoryItem.builder();
            int itemId = Short.toUnsignedInt(packet.readShort());
            ItemDef itemDef = EntityService.getItemDef(itemId);
            boolean wielded = packet.readByte() == 1;
            boolean noted = packet.readByte() == 1;

            itemBuilder.def(itemDef);
            itemBuilder.wielded(wielded);
            itemBuilder.noted(noted);
            if (itemDef.isStackable() || noted) {
                int amount = packet.readInt();
                itemBuilder.amount(amount);
            }

            inventoryItems.add(itemBuilder.build());
        }

        return SendInventoryEvent.builder().inventoryItems(inventoryItems).build();
    }
}
