You will need azul zulu jdk15, gradle, and node/yarn installed to run this project


To run the project, in terminal run the following command in the parent directory

`./gradlew bootRun`

Next, in another terminal window, navigate to the orbs-web directory and run the following commands

`yarn`
`yarn start`
